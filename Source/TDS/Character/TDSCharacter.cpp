#include "TDSCharacter.h"
#include "StateEffect/TDS_StateEffect_Electric.h"

ATDSCharacter::ATDSCharacter()
{
    // Set size for player capsule
    GetCapsuleComponent()->InitCapsuleSize(42.0f, 96.0f);

    // Don't rotate character to camera direction
    bUseControllerRotationPitch = false;
    bUseControllerRotationYaw = false;
    bUseControllerRotationRoll = false;

    // Configure character movement
    GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
    GetCharacterMovement()->RotationRate = FRotator(0.0f, 640.0f, 0.0f);
    GetCharacterMovement()->bConstrainToPlane = true;
    GetCharacterMovement()->bSnapToPlaneAtStart = true;

    // Create a camera boom...
    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->SetupAttachment(RootComponent);
    CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
    CameraBoom->TargetArmLength = 800.0f;
    CameraBoom->SetRelativeRotation(FRotator(-60.0f, 0.0f, 0.0f));
    CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

    // Create a camera...
    TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
    TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
    TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

// VComponents
    InventoryComponent = CreateDefaultSubobject<UTDSInventoryComponent>("InventoryComponent");
    HealthComponent = CreateDefaultSubobject<UTDSCharacterHealthComponent>("HealthComponent");

    if (InventoryComponent)
    {
        InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDSCharacter::SpawnWeapon);
    }

    if (HealthComponent)
    {
        HealthComponent->OnDead.AddDynamic(this, &ATDSCharacter::CharacterDead);
    }

    PrimaryActorTick.bCanEverTick = true;
    PrimaryActorTick.bStartWithTickEnabled = true;

    //Network
    bReplicates = true;
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    if (CurrentCursor)
    {
        APlayerController *MyPlayerController = Cast<APlayerController>(GetController());
        if (MyPlayerController && MyPlayerController->IsLocalPlayerController())
        {
            FHitResult TraceHitResult;
            MyPlayerController->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
            FVector CursorFv = TraceHitResult.ImpactNormal;
            FRotator CursorR = CursorFv.Rotation();

            CurrentCursor->SetWorldLocation(TraceHitResult.Location);
            CurrentCursor->SetWorldRotation(CursorR);
        }
    }

    MovementTick(DeltaSeconds);
}

void ATDSCharacter::BeginPlay()
{
    Super::BeginPlay();

    if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
    {
        if (CursorMaterial && GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority)
        {
            CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
        }
    }
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::InputAxisX);
    PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::InputAxisY);

    PlayerInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDSCharacter::FireStart);
    PlayerInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::FireStop);
    PlayerInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TryReloadWeapon);

    PlayerInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TrySwitchNextWeapon);
    PlayerInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TrySwitchPreviosWeapon);
    PlayerInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TryAbilityEnabled);

    PlayerInputComponent->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Pressed, this, &ATDSCharacter::DropCurrentWeapon);

// -------------------------------------------------- HotKey -------------------------------------------------
    TArray<FKey> HotKeys;
    HotKeys.Add(EKeys::One);
    HotKeys.Add(EKeys::Two);
    HotKeys.Add(EKeys::Three);
    HotKeys.Add(EKeys::Four);
    HotKeys.Add(EKeys::Five);
    HotKeys.Add(EKeys::Six);
    HotKeys.Add(EKeys::Seven);
    HotKeys.Add(EKeys::Eight);
    HotKeys.Add(EKeys::Nine);
    HotKeys.Add(EKeys::Zero);

    PlayerInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &ATDSCharacter::TKeyPressed<1>);
    PlayerInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &ATDSCharacter::TKeyPressed<2>);
    PlayerInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &ATDSCharacter::TKeyPressed<3>);
    PlayerInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &ATDSCharacter::TKeyPressed<4>);
    PlayerInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &ATDSCharacter::TKeyPressed<5>);
    PlayerInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &ATDSCharacter::TKeyPressed<6>);
    PlayerInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &ATDSCharacter::TKeyPressed<7>);
    PlayerInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &ATDSCharacter::TKeyPressed<8>);
    PlayerInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &ATDSCharacter::TKeyPressed<9>);
    PlayerInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &ATDSCharacter::TKeyPressed<0>);
}
// -------------------------------------------------- Multiplayer --------------------------------------------
void ATDSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ATDSCharacter, MovementState);
    DOREPLIFETIME(ATDSCharacter, CurrentWeapon);
    DOREPLIFETIME(ATDSCharacter, CurrentIndexWeapon);
    DOREPLIFETIME(ATDSCharacter, UsedEffects);
    DOREPLIFETIME(ATDSCharacter, EffectAdd);
    DOREPLIFETIME(ATDSCharacter, EffectRemove);
}

// -------------------------------------------------- HotKey -------------------------------------------------
void ATDSCharacter::TrySwitchWeaponToIndexByKeyInput_OnServer_Implementation(int32 ToIndex)
{
    bool bIsSuccess = false;
    if (InventoryComponent->WeaponSlots.IsValidIndex(ToIndex) && CurrentIndexWeapon != ToIndex && InventoryComponent && CurrentWeapon && !CurrentWeapon->bWeaponReloading)
    {
        int32 OldIndex = CurrentIndexWeapon;
        FAdditionalWeaponInfo OldInfo;

        if (CurrentWeapon)
        {
            OldInfo = CurrentWeapon->AdditionalWeaponInfo;
            if (CurrentWeapon->bWeaponReloading)
            {
                CurrentWeapon->CancelReload();
            }
        }
        bIsSuccess = InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
    }
}

// -------------------------------------------------- Movement --------------------------------------------------
void ATDSCharacter::InputAxisX(float Value)
{
    AxisX = Value;
}

void ATDSCharacter::InputAxisY(float Value)
{
    AxisY = Value;
}

void ATDSCharacter::MovementTick(float DeltaTime)
{
    if (HealthComponent && HealthComponent->GetIsAlive())
    {
        if (GetController() && GetController()->IsLocalPlayerController())
        {
            AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
            AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

            if (MovementState == EMovementState::SprintRun_State)
            {
                FVector MyRotationVector = FVector(AxisX, AxisY, 0.0f);
                FRotator MyRotator = MyRotationVector.ToOrientationRotator();

                SetActorRotation((FQuat(MyRotator)));
                SetActorRotationByYaw_OnServer(MyRotator.Yaw);
            }
            else
            {
                APlayerController *MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
                if (MyController)
                {
                    FHitResult ResultHit;
                    MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

                    float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
                    SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));
                    SetActorRotationByYaw_OnServer(FindRotatorResultYaw);

                    if (CurrentWeapon)
                    {
                        FVector Displacement = FVector(0);
                        bool bIsReduceDispersion = false;

                        switch (MovementState)
                        {
                        case EMovementState::Aim_State:
                            Displacement = FVector(0.0f, 0.0f, 160.0f);
                            bIsReduceDispersion = true;
                            break;
                        case EMovementState::AimWalk_State:
                            Displacement = FVector(0.0f, 0.0f, 160.0f);
                            bIsReduceDispersion = true;
                            break;
                        case EMovementState::Walk_State:
                            Displacement = FVector(0.0f, 0.0f, 120.0f);
                            break;
                        case EMovementState::Run_State:
                            Displacement = FVector(0.0f, 0.0f, 120.0f);
                            break;
                        case EMovementState::SprintRun_State:
                            break;
                        default:
                            break;
                        }
                        CurrentWeapon->UpdateWeaponByCharacterMovementState_OnServer(ResultHit.Location + Displacement, bIsReduceDispersion);
                    }
                }
            }
        }
    }
}

void ATDSCharacter::ChangeMovementState()
{
    EMovementState NewState = EMovementState::Run_State;
    // Run
    if (!bWalkEnabled && !bSprintRunEnabled && !bAimEnabled)
    {
        NewState = EMovementState::Run_State;
    }
    else
    {
        // Sprint
        if (bSprintRunEnabled)
        {
            bWalkEnabled = false;
            bAimEnabled = false;
            NewState = EMovementState::SprintRun_State;
        }
        else
        {
            // Aim Walk
            if (bWalkEnabled && !bSprintRunEnabled && bAimEnabled)
            {
                NewState = EMovementState::AimWalk_State;
            }
            else
            {
                // Walk
                if (bWalkEnabled && !bSprintRunEnabled && !bAimEnabled)
                {
                    NewState = EMovementState::Walk_State;
                }
                else
                {
                    // Aim
                    if (!bWalkEnabled && !bSprintRunEnabled && bAimEnabled)
                    {
                        NewState = EMovementState::Aim_State;
                    }
                }
            }
        }
    }

    SetMovementState_OnServer(NewState);

    // Weapon state update
    AWeaponDefault *MyWeapon = GetCurrentWeapon();
    if (MyWeapon)
    {
        MyWeapon->UpdateStateWeapon_OnServer(NewState);
    }
}

void ATDSCharacter::CharacterUpdate()
{
    float ResultSpeed = 600.0f;
    switch (MovementState)
    {
    case EMovementState::Aim_State:
        ResultSpeed = MovementSpeedInfo.AimSpeedNormal;
        break;
    case EMovementState::AimWalk_State:
        ResultSpeed = MovementSpeedInfo.AimWalkSpeed;
        break;
    case EMovementState::Walk_State:
        ResultSpeed = MovementSpeedInfo.WalkSpeedNormal;
        break;
    case EMovementState::Run_State:
        ResultSpeed = MovementSpeedInfo.RunSpeedNormal;
        break;
    case EMovementState::SprintRun_State:
        ResultSpeed = MovementSpeedInfo.SprintRunSpeed;
        break;
    default:
        break;
    }

    GetCharacterMovement()->MaxWalkSpeed = ResultSpeed;
}

void ATDSCharacter::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
    SetActorRotationByYaw_Multicast(Yaw);
}

void ATDSCharacter::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
    if (Controller && !Controller->IsLocalPlayerController())
    {
        SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
    }
}

void ATDSCharacter::SetMovementState_OnServer_Implementation(EMovementState NewState)
{
    SetMovementState_Multicast(NewState);
}

void ATDSCharacter::SetMovementState_Multicast_Implementation(EMovementState NewState)
{
    MovementState = NewState;
    CharacterUpdate();
}

// -------------------------------------------------- Cursor ----------------------------------------------------
UDecalComponent *ATDSCharacter::GetCursorToWorld()
{
    return CurrentCursor;
}

// -------------------------------------------------- For Weapon ------------------------------------------------
void ATDSCharacter::SpawnWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
    // On Server
    if (CurrentWeapon)
    {
        CurrentWeapon->Destroy();
        CurrentWeapon = nullptr;
    }

    UTDSGameInstance *MyGameInstance = Cast<UTDSGameInstance>(GetGameInstance());
    FWeaponInfo MyWeaponInfo;
    if (MyGameInstance)
    {
        if (MyGameInstance->GetWeaponInfoByName(IdWeaponName, MyWeaponInfo))
        {
            if (MyWeaponInfo.WeaponClass)
            {
                FVector SpawnLocation = FVector(0);
                FRotator SpawnRotation = FRotator(0);

                FActorSpawnParameters SpawnParams;
                SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
                SpawnParams.Owner = this;
                SpawnParams.Instigator = GetInstigator();

                AWeaponDefault *MyWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(MyWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
                if (MyWeapon)
                {
                    FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
                    MyWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
                    CurrentWeapon = MyWeapon;

                    MyWeapon->IdWeaponName = IdWeaponName;
                    MyWeapon->WeaponSetting = MyWeaponInfo;

                    MyWeapon->ReloadTime = MyWeaponInfo.ReloadTime;
                    MyWeapon->UpdateStateWeapon_OnServer(MovementState);

                    MyWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
                    CurrentIndexWeapon = NewCurrentIndexWeapon;

                    MyWeapon->OnWeaponFireStart.AddDynamic(this, &ATDSCharacter::WeaponFireStart);
                    MyWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDSCharacter::WeaponReloadStart);
                    MyWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDSCharacter::WeaponReloadEnd);

                    if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
                    {
                        CurrentWeapon->InitReload();
                    }
                }
            }
        }
        else
        {
            UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::SpawnWeapon - Weapon not found is table - NULL"));
        }
    }
}

AWeaponDefault *ATDSCharacter::GetCurrentWeapon()
{
    return CurrentWeapon;
}

int32 ATDSCharacter::GetCurrentWeaponIndex()
{
    return CurrentIndexWeapon;
}

void ATDSCharacter::AttackCharEvent(bool bIsFiring)
{
    AWeaponDefault *MyWeapon = nullptr;
    MyWeapon = GetCurrentWeapon();
    if (MyWeapon)
    {
        MyWeapon->SetWeaponStateFire_OnServer(bIsFiring);
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
    }
}

// -------------------------------------------------- Weapon For Inventory --------------------------------------
void ATDSCharacter::TrySwitchNextWeapon()
{
    if (CurrentWeapon && !CurrentWeapon->bWeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
    {
        int32 OldIndex = CurrentIndexWeapon;
        FAdditionalWeaponInfo OldInfo;
        if (CurrentWeapon)
        {
            OldInfo = CurrentWeapon->AdditionalWeaponInfo;
            if (CurrentWeapon->bWeaponReloading)
            {
                CurrentWeapon->CancelReload();
            }
        }

        if (InventoryComponent)
        {
            if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
            { }
        }
    }
}

void ATDSCharacter::TrySwitchPreviosWeapon()
{
    if (CurrentWeapon && !CurrentWeapon->bWeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
    {
        int32 OldIndex = CurrentIndexWeapon;
        FAdditionalWeaponInfo OldInfo;
        if (CurrentWeapon)
        {
            OldInfo = CurrentWeapon->AdditionalWeaponInfo;
            if (CurrentWeapon->bWeaponReloading)
            {
                CurrentWeapon->CancelReload();
            }
        }

        if (InventoryComponent)
        {
            if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
            { }
        }
    }
}

// -------------------------------------------------- Fire ------------------------------------------------------
void ATDSCharacter::FireStart()
{
    if (HealthComponent && HealthComponent->GetIsAlive())
    {
        AttackCharEvent(true);
    }
}

void ATDSCharacter::FireStop()
{
    AttackCharEvent(false);
}

void ATDSCharacter::WeaponFireStart(UAnimMontage* AnimFireChar)
{
    if (InventoryComponent && CurrentWeapon)
    {
        InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon,CurrentWeapon->AdditionalWeaponInfo);
    }
    WeaponFireStart_BP(AnimFireChar);
}

void ATDSCharacter::WeaponFireStart_BP_Implementation(UAnimMontage *AnimFireChar)
{
    // in BP
}

// -------------------------------------------------- Reload ----------------------------------------------------
void ATDSCharacter::TryReloadWeapon()
{
    if (HealthComponent && HealthComponent->GetIsAlive() && CurrentWeapon && !CurrentWeapon->bWeaponReloading)
    {
        TryReloadWeapon_OnServer();
    }
}

void ATDSCharacter::TryReloadWeapon_OnServer_Implementation()
{
    if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
    {
        CurrentWeapon->InitReload();
    }
}

void ATDSCharacter::WeaponReloadStart(UAnimMontage* AnimReloadChar)
{
    WeaponReloadStart_BP(AnimReloadChar);
}

void ATDSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* AnimReloadChar)
{
    // in BP
}

void ATDSCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
    if(InventoryComponent && CurrentWeapon)
    {
        InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
        InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
    }
    WeaponReloadEnd_BP(bIsSuccess);
}

void ATDSCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
    // in BP
}

// -------------------------------------------------- Health Component ------------------------------------------
bool ATDSCharacter::GetIsAlive()
{
    bool bResult = false;
    if (HealthComponent)
    {
        bResult = HealthComponent->GetIsAlive();
    }
    return bResult;
}

float ATDSCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const &DamageEvent, class AController *EventInstigator, AActor *DamageCauser)
{
    float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

    if (HealthComponent && HealthComponent->GetIsAlive())
    {
        HealthComponent->ChangeCurrentHealth_OnServer(-ActualDamage);
    }

    if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
    {
        AProjectileDefault *MyProjectile = Cast<AProjectileDefault>(DamageCauser);
        if (MyProjectile)
        {
            UTypes::AddEffectBySurfaceType(this, NAME_None, MyProjectile->ProjectileSetting.StateEffect, GetSurfaceType());
        }
    }
    else
    {
        ATDS_StateEffect_Electric *StateEffect_Electric = Cast<ATDS_StateEffect_Electric>(DamageCauser);
        if (StateEffect_Electric)
        {
            UTypes::AddEffectBySurfaceType(this, NAME_None, StateEffect_Electric->StateEffect_Electric, GetSurfaceType());
        }
    }

    return ActualDamage;
}

void ATDSCharacter::CharacterDead()
{
    CharacterDead_BP();
    if (HasAuthority())
    {
        // A������� ������
        float TimeAnim = 0.0f;
        int32 RandomIndex = FMath::RandHelper(DeadsAnim.Num());
        if (DeadsAnim.IsValidIndex(RandomIndex) && DeadsAnim[RandomIndex] && GetMesh() && GetMesh()->GetAnimInstance())
        {
            TimeAnim = DeadsAnim[RandomIndex]->GetPlayLength();
            PlayAnim_Multicast(DeadsAnim[RandomIndex]);
        }

        if (GetController())
        {
            GetController()->UnPossess();
        }

        // Timer rag doll
        GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATDSCharacter::EnableRagdoll_Multicast, TimeAnim, false);

        SetLifeSpan(20.f);
        if (GetCurrentWeapon())
        {
            GetCurrentWeapon()->SetLifeSpan(20.f);
        }
    }
    else
    {
        if (GetCursorToWorld())
        {
            GetCursorToWorld()->SetVisibility(false);
        }

        // stop fire
        AttackCharEvent(false);
    }
}

void ATDSCharacter::CharacterDead_BP_Implementation()
{
    // in BP;
}

void ATDSCharacter::EnableRagdoll_Multicast_Implementation()
{
    if (GetMesh())
    {
        if (GetCapsuleComponent())
        {
            GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
        }
        GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
        GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Block);
        GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
        GetMesh()->SetSimulatePhysics(true);
    }
}

void ATDSCharacter::PlayAnim_Multicast_Implementation(UAnimMontage *Anim)
{
    if (GetMesh() && GetMesh()->GetAnimInstance())
    {
        GetMesh()->GetAnimInstance()->Montage_Play(Anim);
    }
}

// -------------------------------------------------- State Effect -----------------------------------------------
EPhysicalSurface ATDSCharacter::GetSurfaceType()
{
    EPhysicalSurface Result = EPhysicalSurface ::SurfaceType_Default;
    if (HealthComponent)
    {
        if (HealthComponent->GetCurrentShield() <= 0)
        {
            if (GetMesh())
            {
                UMaterialInterface *MyMaterial = GetMesh()->GetMaterial(0);
                if (MyMaterial)
                {
                    Result = MyMaterial->GetPhysicalMaterial()->SurfaceType;
                }
            }
        }
    }

    return Result;
}

void ATDSCharacter::AddEffect_Implementation(UTDS_StateEffect *NewEffect)
{
    UsedEffects.Add(NewEffect);

    if (!NewEffect->bIsAutoDestroyParticleEffect)
    {
        SwitchEffect(NewEffect, true);
        EffectAdd = NewEffect;
    }
    else
    {
        if (NewEffect->ParticleEffect)
        {
            ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
        }
    }
}

void ATDSCharacter::RemoveEffect_Implementation(UTDS_StateEffect *RemoveEffect)
{
    UsedEffects.Remove(RemoveEffect);

    if (!RemoveEffect->bIsAutoDestroyParticleEffect)
    {
        SwitchEffect(RemoveEffect, false);
        EffectRemove = RemoveEffect;
    }
}

void ATDSCharacter::EffectAdd_OnRep()
{
    if (EffectAdd)
    {
        SwitchEffect(EffectAdd, true);
    }
}

void ATDSCharacter::EffectRemove_OnRep()
{
    if (EffectRemove)
    {
        SwitchEffect(EffectRemove, false);
    }
}

void ATDSCharacter::SwitchEffect(UTDS_StateEffect *Effect, bool bIsAdd)
{
    if (Effect && Effect->ParticleEffect)
    {
        if (bIsAdd)
        {
            if (Effect && Effect->ParticleEffect)
            {
                FName NameBoneToAttached = Effect->NameBone;
                FVector LocationEffect = FVector(0);

                USkeletalMeshComponent *MySkeletalMesh = GetMesh();
                if (MySkeletalMesh)
                {
                    UParticleSystemComponent *NewParticleSystem = UGameplayStatics::SpawnEmitterAttached(
                        Effect->ParticleEffect, MySkeletalMesh, NameBoneToAttached, LocationEffect,
                        FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
                    ParticleSystemEffects.Add(NewParticleSystem);
                }
            }
        }
        else
        {
            int32 i = 0;
            bool bIsFind = false;
            if (ParticleSystemEffects.Num() > 0)
            {
                while (i < ParticleSystemEffects.Num() && !bIsFind)
                {
                    if (ParticleSystemEffects[i] && ParticleSystemEffects[i]->Template && Effect->ParticleEffect &&
                        Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
                    {
                        bIsFind = true;
                        ParticleSystemEffects[i]->DeactivateSystem();
                        ParticleSystemEffects[i]->DestroyComponent();
                        ParticleSystemEffects.RemoveAt(i);
                    }
                    i++;
                }
            }
        }
    }
}

void ATDSCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem *ExecuteFX)
{
    ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATDSCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem *ExecuteFX)
{
    UTypes::ExecuteEffectAdded(ExecuteFX, this, FVector(0), FName("Spine_01"));
}

bool ATDSCharacter::ReplicateSubobjects(UActorChannel *Channel, FOutBunch *Bunch, FReplicationFlags *RepFlags)
{
    bool bWrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

    for (int32 i = 0; i < UsedEffects.Num(); i++)
    {
        if (UsedEffects[i])
        {
            bWrote |= Channel->ReplicateSubobject(UsedEffects[i], *Bunch, *RepFlags);
        }
    }
    return bWrote;
}

TArray<UTDS_StateEffect*> ATDSCharacter::GetAllCurrentEffects()
{
    return UsedEffects;
}

void ATDSCharacter::TryAbilityEnabled()
{
    if (AbilityEffect)
    {
        UTDS_StateEffect *NewEffect = NewObject<UTDS_StateEffect>(this, AbilityEffect);
        if (NewEffect)
        {
            NewEffect->InitObject(this, NAME_None);
        }
    }
}

// -------------------------------------------------- Drop Item To World ---------------------------------------
void ATDSCharacter::DropCurrentWeapon()
{
    if (InventoryComponent)
    {
        InventoryComponent->DropWeaponByIndex_OnServer(CurrentIndexWeapon);
    }
}