#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FuncLibrary/Types.h"
#include "Components/CapsuleComponent.h"
#include "Components/DecalComponent.h"
#include "Character/TDSInventoryComponent.h"
#include "Character/TDSCharacterHealthComponent.h"
#include "Weapon/WeaponDefault.h"
#include "Weapon/Projectiles/ProjectileDefault.h"
#include "Camera/CameraComponent.h"
#include "Engine/World.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Materials/Material.h"
#include "Game/TDSGameInstance.h"
#include "UObject/ConstructorHelpers.h"
#include "Interface/TDS_IGameActor.h"
#include "Net/UnrealNetwork.h"
#include "Engine/ActorChannel.h"
#include "TDSCharacter.generated.h"

class UTDSInventoryComponent;


UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter, public ITDS_IGameActor
{
    GENERATED_BODY()

protected:
    virtual void BeginPlay() override;

public:
    ATDSCharacter();

    virtual void Tick(float DeltaSeconds) override;

    virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;

/** Returns TopDownCameraComponent subobject **/
    FORCEINLINE class UCameraComponent *GetTopDownCameraComponent() const
    {
        return TopDownCameraComponent;
    }
/** Returns CameraBoom subobject **/
    FORCEINLINE class USpringArmComponent *GetCameraBoom() const
    {
        return CameraBoom;
    }

// -------------------------------------------------- Components ---------------------------------------------
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    UTDSInventoryComponent *InventoryComponent;
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
    UTDSCharacterHealthComponent *HealthComponent;

private:
/** Top down camera */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UCameraComponent *TopDownCameraComponent;

/** Camera boom positioning the camera above the character */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class USpringArmComponent *CameraBoom;

public:
// -------------------------------------------------- Multiplayer --------------------------------------------
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const override;

// -------------------------------------------------- HotKey -------------------------------------------------
    template<int32 Id>
    void TKeyPressed()
    {
        TrySwitchWeaponToIndexByKeyInput_OnServer(Id);
    }

    UFUNCTION(Server, Reliable)
    void TrySwitchWeaponToIndexByKeyInput_OnServer(int32 ToIndex);

// -------------------------------------------------- Movement -------------------------------------------------
    float AxisX = 0.0f;
    float AxisY = 0.0f;
    UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Movement")
    EMovementState MovementState = EMovementState::Run_State;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    FCharacterSpeed MovementSpeedInfo;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    bool bSprintRunEnabled = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    bool bWalkEnabled = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    bool bAimEnabled = false;

    UFUNCTION()
    void InputAxisX(float Value);
    UFUNCTION()
    void InputAxisY(float Value);
    UFUNCTION()
    void MovementTick(float DeltaTime);
    UFUNCTION(BlueprintCallable)
    void ChangeMovementState();
    UFUNCTION(BlueprintCallable)
    void CharacterUpdate();

    UFUNCTION(Server, Unreliable)
    void SetActorRotationByYaw_OnServer(float Yaw);
    UFUNCTION(NetMulticast, Unreliable)
    void SetActorRotationByYaw_Multicast(float Yaw);

    UFUNCTION(Server, Reliable)
    void SetMovementState_OnServer(EMovementState NewState);
    UFUNCTION(NetMulticast, Reliable)
    void SetMovementState_Multicast(EMovementState NewState);

// -------------------------------------------------- Cursor ----------------------------------------------------
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
    UMaterialInterface *CursorMaterial = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
    FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
    UDecalComponent *CurrentCursor = nullptr;

    UFUNCTION(BlueprintCallable)
    UDecalComponent *GetCursorToWorld();

// -------------------------------------------------- For Weapon ------------------------------------------------
    UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
    AWeaponDefault *CurrentWeapon = nullptr;
    UPROPERTY(Replicated, BlueprintReadOnly, EditDefaultsOnly, Category = "Weapon")
    int32 CurrentIndexWeapon = 0;

    UFUNCTION(BlueprintCallable)
    void SpawnWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
    UFUNCTION(BlueprintCallable, BlueprintPure)
    AWeaponDefault *GetCurrentWeapon();
    UFUNCTION(BlueprintCallable, BlueprintPure)
    int32 GetCurrentWeaponIndex();
    UFUNCTION(BlueprintCallable)
    void AttackCharEvent(bool bIsFiring);

// -------------------------------------------------- Weapon For Inventory --------------------------------------
    void TrySwitchNextWeapon();
    void TrySwitchPreviosWeapon();

// -------------------------------------------------- Fire ------------------------------------------------------
    UFUNCTION()
    void FireStart();
    UFUNCTION()
    void FireStop();
    UFUNCTION()
    void WeaponFireStart(UAnimMontage *AnimFireChar);
    UFUNCTION(BlueprintNativeEvent)
    void WeaponFireStart_BP(UAnimMontage *AnimFireChar);

// -------------------------------------------------- Reload ----------------------------------------------------
    void TryReloadWeapon();
    UFUNCTION(Server, Reliable)
    void TryReloadWeapon_OnServer();
    UFUNCTION()
    void WeaponReloadStart(UAnimMontage* AnimReloadChar);
    UFUNCTION(BlueprintNativeEvent)
    void WeaponReloadStart_BP(UAnimMontage* AnimReloadChar);
    UFUNCTION()
    void WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake);
    UFUNCTION(BlueprintNativeEvent)
    void WeaponReloadEnd_BP(bool bIsSuccess);

// -------------------------------------------------- Health Component ------------------------------------------
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
    TArray<UAnimMontage *> DeadsAnim;
    FTimerHandle TimerHandle_RagDollTimer;

    UFUNCTION(BlueprintCallable, BlueprintPure)
    bool GetIsAlive();
    float TakeDamage(float DamageAmount, struct FDamageEvent const &DamageEvent, class AController *EventInstigator, AActor *DamageCauser) override;
    UFUNCTION()
    void CharacterDead();
    UFUNCTION(BlueprintNativeEvent)
    void CharacterDead_BP();
    UFUNCTION(NetMulticast, Reliable)
    void EnableRagdoll_Multicast();
    UFUNCTION(NetMulticast, Reliable)
    void PlayAnim_Multicast(UAnimMontage* Anim);

    // -------------------------------------------------- State Effect -----------------------------------------------
    UPROPERTY(Replicated)
    TArray<UTDS_StateEffect *> UsedEffects;
    UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
    UTDS_StateEffect *EffectAdd = nullptr;
    UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
    UTDS_StateEffect *EffectRemove = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
    TArray<UParticleSystemComponent *> ParticleSystemEffects;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
    TSubclassOf<UTDS_StateEffect> AbilityEffect;
    bool bOverlap_Electric = false;

    EPhysicalSurface GetSurfaceType() override;
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    void AddEffect(UTDS_StateEffect *NewEffect);
    void AddEffect_Implementation(UTDS_StateEffect *NewEffect) override;
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    void RemoveEffect(UTDS_StateEffect *RemoveEffect);
    void RemoveEffect_Implementation(UTDS_StateEffect *RemoveEffect) override;
    UFUNCTION()
    void EffectAdd_OnRep();
    UFUNCTION()
    void EffectRemove_OnRep();
    UFUNCTION()
    void SwitchEffect(UTDS_StateEffect *Effect, bool bIsAdd);
    UFUNCTION(Server, Reliable)
    void ExecuteEffectAdded_OnServer(UParticleSystem *ExecuteFX);
    UFUNCTION(NetMulticast, Reliable)
    void ExecuteEffectAdded_Multicast(UParticleSystem *ExecuteFX);
    bool ReplicateSubobjects(UActorChannel *Channel, FOutBunch *Bunch, FReplicationFlags *RepFlags) override;
    TArray<UTDS_StateEffect *> GetAllCurrentEffects() override;
    void TryAbilityEnabled();

// -------------------------------------------------- Drop Item To World ---------------------------------------
    void DropCurrentWeapon();
};
