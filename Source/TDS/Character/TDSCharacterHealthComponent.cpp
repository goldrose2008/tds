#include "Character/TDSCharacterHealthComponent.h"

// -------------------------------------------------- Delegate ---------------------------------------------------------------------
void UTDSCharacterHealthComponent::OnShieldChangeEvent_Multicast_Implementation(float ValueShield, float Damage)
{
    OnShieldChange.Broadcast(ValueShield, Damage);
}

// -------------------------------------------------- Change Health ----------------------------------------------------------------
void UTDSCharacterHealthComponent::ChangeCurrentHealth_OnServer(float ChangeValue)
{
    float CurrentDamage = ChangeValue * CoefDamage;

    if (Shield > 0.0f && ChangeValue < 0.0f)
    {
        ChangeCurrentShield(CurrentDamage);  // � ��������� ������ ChangeValue
        if (Shield <= 0.0f)
        {
            // FX
        }
    }
    else
    {
        Super::ChangeCurrentHealth_OnServer(CurrentDamage); // � ��������� ������ ChangeValue
    }
}

// -------------------------------------------------- Shield -----------------------------------------------------------------------
float UTDSCharacterHealthComponent::GetCurrentShield()
{
    return Shield;
}

void UTDSCharacterHealthComponent::ChangeCurrentShield(float ChangeValue)
{
    float CurrentShield = GetCurrentShield() + ChangeValue;
    OnShieldChangeEvent_Multicast(CurrentShield, ChangeValue);

    if (CurrentShield > 0.0f)
    {
        Shield = CurrentShield;

        if (Shield >= 100.0f)
        {
            Shield = 100.0f;
        }
    }
    else
    {
        if (CurrentShield <= 0.0f)
        {
            Shield = 0.0f;
            ChangeCurrentHealth_OnServer(CurrentShield);
        }
    }

    if (GetWorld())
    {
        GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoollDownShieldTimer, this, &UTDSCharacterHealthComponent::CoollDownShieldEnd, CoollDownShieldRecoverTime, false);
        GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
    }
}

void UTDSCharacterHealthComponent::CoollDownShieldEnd()
{
    if (GetWorld())
    {
        GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTDSCharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
    }
}

void UTDSCharacterHealthComponent::RecoveryShield()
{
    float tmp = Shield;
    tmp += ShieldRecoverValue;

    if (tmp > 100.0f)
    {
        Shield = 100.0f;

        GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
    }
    else
    {
        Shield = tmp;
    }

    OnShieldChangeEvent_Multicast(Shield, ShieldRecoverValue);
}