#pragma once

#include "CoreMinimal.h"
#include "Character/TDSHealthComponent.h"
#include "TDSCharacterHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS()
class TDS_API UTDSCharacterHealthComponent : public UTDSHealthComponent
{
	GENERATED_BODY()
	
protected:

public:
// -------------------------------------------------- Delegate ---------------------------------------------------------------------
    UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Delegate")
    FOnShieldChange OnShieldChange;

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Delegate")
    void OnShieldChangeEvent_Multicast(float ValueShield, float Damage);

    // -------------------------------------------------- Change Health -------------------------------------------------------------
    void ChangeCurrentHealth_OnServer(float ChangeValue) override;

    // -------------------------------------------------- Shield --------------------------------------------------------------------
    float Shield = 100.0f;

    FTimerHandle TimerHandle_CoollDownShieldTimer;
    FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
    float CoollDownShieldRecoverTime = 5.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
    float ShieldRecoverValue = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
    float ShieldRecoverRate = 0.1f;

    UFUNCTION(BlueprintCallable, Category = "Shield")
    float GetCurrentShield();
    void ChangeCurrentShield(float ChangeValue);
    void CoollDownShieldEnd();
    void RecoveryShield();
};