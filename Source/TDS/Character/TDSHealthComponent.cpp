#include "Character/TDSHealthComponent.h"

UTDSHealthComponent::UTDSHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

    SetIsReplicatedByDefault(true);
}

void UTDSHealthComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UTDSHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

// -------------------------------------------------- Multiplayer ----------------------------------------------------------
void UTDSHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UTDSHealthComponent, Health);
    DOREPLIFETIME(UTDSHealthComponent, bIsAlive);
}

// -------------------------------------------------- Delegate -------------------------------------------------------------
void UTDSHealthComponent::OnHealthChangeEvent_Multicast_Implementation(float ValueHealth, float Damage)
{
    OnHealthChange.Broadcast(ValueHealth, Damage);
}

void UTDSHealthComponent::OnDeadEvent_Multicast_Implementation()
{
    OnDead.Broadcast();
}

// -------------------------------------------------- Info for Health -------------------------------------------------------
float UTDSHealthComponent::GetCurrentHealth()
{
    return Health;
}

void UTDSHealthComponent::SetCurrentHealth(float NewHealth)
{
    Health = NewHealth;
}

bool UTDSHealthComponent::GetIsAlive()
{
    return bIsAlive;
}

// -------------------------------------------------- Change Health ---------------------------------------------------------
void UTDSHealthComponent::ChangeCurrentHealth_OnServer_Implementation(float ChangeValue)
{
    if (bIsAlive)
    {
        ChangeValue *= CoefDamage;
        Health += ChangeValue;
        OnHealthChangeEvent_Multicast(Health, ChangeValue);

        if (Health >= 100.0f)
        {
            Health = 100.0f;
        }
        else
        {
            if (Health <= 0.0f)
            {
                bIsAlive = false;
                OnDeadEvent_Multicast();
            }
        }
    }
}
