#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Net/UnrealNetwork.h"
#include "TDSHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_API UTDSHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
    UTDSHealthComponent();

// -------------------------------------------------- Multiplayer ----------------------------------------------------------
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const override;

// -------------------------------------------------- Delegate -------------------------------------------------------------
    UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Delegate")
    FOnHealthChange OnHealthChange;
    UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Delegate")
    FOnDead OnDead;

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Delegate")
    void OnHealthChangeEvent_Multicast(float ValueHealth, float Damage);
    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Delegate")
    void OnDeadEvent_Multicast();

  protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

// -------------------------------------------------- Info for Health -------------------------------------------------------
    UPROPERTY(Replicated)
    float Health = 100.0f;
    UPROPERTY(Replicated)
    bool bIsAlive = true;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
    float CoefDamage = 1.0f;

    UFUNCTION(BlueprintCallable, Category = "Health")
    float GetCurrentHealth();
    UFUNCTION(BlueprintCallable, Category = "Health")
    void SetCurrentHealth(float NewHealth);
    UFUNCTION(BlueprintCallable, Category = "Health")
    bool GetIsAlive();

// -------------------------------------------------- Change Health ---------------------------------------------------------
    UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Health")
    virtual void ChangeCurrentHealth_OnServer(float ChangeValue);
};