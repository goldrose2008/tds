#include "Character/TDSInventoryComponent.h"

UTDSInventoryComponent::UTDSInventoryComponent()
{
    PrimaryComponentTick.bCanEverTick = true;

    SetIsReplicatedByDefault(true);
}

void UTDSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

    //InitInventory(WeaponSlots, AmmoSlots);
}

void UTDSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

// -------------------------------------------------- For Game -------------------------------------------------
UTDSGameInstance* UTDSInventoryComponent::GetGameInstance()
{
    return Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
}

// -------------------------------------------------- Multiplayer --------------------------------------------
void UTDSInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UTDSInventoryComponent, WeaponSlots);
    DOREPLIFETIME(UTDSInventoryComponent, AmmoSlots);
}

// -------------------------------------------------- Switch Weapon --------------------------------------------
bool UTDSInventoryComponent::SwitchWeaponToIndexByNextPreviosIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward)
{
    bool bIsSuccess = false;
    int32 CorrectIndex = ChangeToIndex;
	if (ChangeToIndex > WeaponSlots.Num() - 1)
	{
        CorrectIndex = 0;
	}
	else
	{
		if (ChangeToIndex < 0)
		{
            CorrectIndex = WeaponSlots.Num() - 1;
		}
	}

	FName NewIdWeapon;
    FAdditionalWeaponInfo NewAdditionalInfo;
    int32 NewCurrentIndex = 0;

    if (WeaponSlots.IsValidIndex(CorrectIndex))
    {
        if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
        {
            if (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0)
            {
                bIsSuccess = true;
            }
            else
            {
                UTDSGameInstance *MyGameInstance = GetGameInstance();
                if (MyGameInstance)
                {
                    FWeaponInfo MyInfo;
                    MyGameInstance->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, MyInfo);

                    bool bIsFind = false;
                    int8 j = 0;
                    while (j < WeaponSlots.Num() && !bIsFind)
                    {
                        if (AmmoSlots[j].WeaponType == MyInfo.WeaponType && AmmoSlots[j].Cout > 0)
                        {
                            bIsSuccess = true;
                            bIsFind = true;
                        }
                        j++;
                    }
                }
            }
            if (bIsSuccess)
            {
                NewCurrentIndex = CorrectIndex;
                NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
                NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
            }
        }
    }
//���� ��� �������� ��� ������� ���� ������
    if (!bIsSuccess)
    {
        int32 Iteration = 0;
        int32 SecondIteration = 0;
        int32 TempIndex = 0;
        while (Iteration < WeaponSlots.Num() && !bIsSuccess)
        {
            Iteration++;
            if (bIsForward)
            {
                TempIndex = ChangeToIndex + Iteration;
            }
            else
            {
                SecondIteration = WeaponSlots.Num() - 1;
                TempIndex = ChangeToIndex - Iteration;
            }

            if (WeaponSlots.IsValidIndex(TempIndex))
            {
                if (!WeaponSlots[TempIndex].NameItem.IsNone())
                {
                    if (WeaponSlots[TempIndex].AdditionalInfo.Round > 0)
                    {
                        bIsSuccess = true;
                        NewIdWeapon = WeaponSlots[TempIndex].NameItem;
                        NewAdditionalInfo = WeaponSlots[TempIndex].AdditionalInfo;
                        NewCurrentIndex = TempIndex;
                    }
                    else
                    {
                        FWeaponInfo MyInfo;
                        UTDSGameInstance *MyGameInstance = GetGameInstance();

                        MyGameInstance->GetWeaponInfoByName(WeaponSlots[TempIndex].NameItem, MyInfo);

                        bool bIsFind = false;
                        int8 j = 0;
                        while (j < AmmoSlots.Num() && !bIsFind)
                        {
                            if (AmmoSlots[j].WeaponType == MyInfo.WeaponType && AmmoSlots[j].Cout > 0)
                            {
                                bIsSuccess = true;
                                NewIdWeapon = WeaponSlots[TempIndex].NameItem;
                                NewAdditionalInfo = WeaponSlots[TempIndex].AdditionalInfo;
                                NewCurrentIndex = TempIndex;
                                bIsFind = true;
                            }
                            j++;
                        }
                    }
                }
            }
            else
            {
                if (OldIndex != SecondIteration)
                {
                    if (WeaponSlots.IsValidIndex(SecondIteration))
                    {
                        if (!WeaponSlots[SecondIteration].NameItem.IsNone())
                        {
                            if (WeaponSlots[SecondIteration].AdditionalInfo.Round > 0)
                            {
                                bIsSuccess = true;
                                NewIdWeapon = WeaponSlots[SecondIteration].NameItem;
                                NewAdditionalInfo = WeaponSlots[SecondIteration].AdditionalInfo;
                                NewCurrentIndex = SecondIteration;
                            }
                            else
                            {
                                FWeaponInfo MyInfo;
                                UTDSGameInstance *MyGameInstance = GetGameInstance();

                                MyGameInstance->GetWeaponInfoByName(WeaponSlots[SecondIteration].NameItem, MyInfo);

                                bool bIsFind = false;
                                int8 j = 0;
                                while (j < AmmoSlots.Num() && !bIsFind)
                                {
                                    if (AmmoSlots[j].WeaponType == MyInfo.WeaponType && AmmoSlots[j].Cout > 0)
                                    {
                                        bIsSuccess = true;
                                        NewIdWeapon = WeaponSlots[SecondIteration].NameItem;
                                        NewAdditionalInfo = WeaponSlots[SecondIteration].AdditionalInfo;
                                        NewCurrentIndex = SecondIteration;
                                        bIsFind = true;
                                    }
                                    j++;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (WeaponSlots.IsValidIndex(SecondIteration))
                    {
                        if (!WeaponSlots[SecondIteration].NameItem.IsNone())
                        {
                            if (WeaponSlots[SecondIteration].AdditionalInfo.Round > 0)
                            {
                                // WeaponGood, it same weapon do nothing
                            }
                            else
                            {
                                FWeaponInfo MyInfo;
                                UTDSGameInstance *MyGameInstance = GetGameInstance();

                                MyGameInstance->GetWeaponInfoByName(WeaponSlots[SecondIteration].NameItem, MyInfo);

                                bool bIsFind = false;
                                int8 j = 0;
                                while (j < AmmoSlots.Num() && !bIsFind)
                                {
                                    if (AmmoSlots[j].WeaponType == MyInfo.WeaponType)
                                    {
                                        if (AmmoSlots[j].Cout > 0)
                                        {
                                            // WeaponGood, it same weapon do nothing
                                        }
                                        else
                                        {
                                            UE_LOG(LogTemp, Error, TEXT("UTDSInventoryComponent::SwitchWeaponToIndex : Init PISTOL NEED!"));
                                        }
                                    }
                                    j++;
                                }
                            }
                        }
                    }
                }
                if (bIsForward)
                {
                    SecondIteration++;
                }
                else
                {
                    SecondIteration--;
                }
            }
        }
    }
	if (bIsSuccess)
	{
        SetAdditionalInfoWeapon(OldIndex, OldInfo);
        OnSwitchWeaponEvent_OnServer(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
	}
	
	return bIsSuccess;
}

bool UTDSInventoryComponent::SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviosIndex, FAdditionalWeaponInfo PreviosWeaponInfo)
{
    bool bIsSuccess = false;
    FName ToSwitchIdWeapon;
    FAdditionalWeaponInfo ToSwitchAdditionalInfo;

    ToSwitchIdWeapon = GetWeaponNameBySlotIndex(IndexWeaponToChange);
    ToSwitchAdditionalInfo = GetAdditionalInfoWeapon(IndexWeaponToChange);

    if (!ToSwitchIdWeapon.IsNone())
    {
        SetAdditionalInfoWeapon(PreviosIndex, PreviosWeaponInfo);
        OnSwitchWeaponEvent_OnServer(ToSwitchIdWeapon, ToSwitchAdditionalInfo, IndexWeaponToChange);

        EWeaponType ToSwitchWeaponType;
        if (GetWeaponTypeByNameWeapon(ToSwitchIdWeapon, ToSwitchWeaponType))
        {
            int32 AviableAmmoForWeapon = -1;
            if (CheckAmmoForWeapon(ToSwitchWeaponType, AviableAmmoForWeapon))
            {

            }
        }
        bIsSuccess = true;
    }
    return bIsSuccess;
}

bool UTDSInventoryComponent::GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType &WeaponType)
{
    bool bIsFind = false;
    FWeaponInfo OutInfo;
    WeaponType = EWeaponType::RifleType;
    UTDSGameInstance *MyGameInstance = GetGameInstance();
    if (MyGameInstance)
    {
        MyGameInstance->GetWeaponInfoByName(IdWeaponName, OutInfo);
        WeaponType = OutInfo.WeaponType;
        bIsFind = true;
    }
    return bIsFind;
}

bool UTDSInventoryComponent::GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType &WeaponType)
{
    bool bIsFind = false;
    FWeaponInfo OutInfo;
    WeaponType = EWeaponType::RifleType;
    UTDSGameInstance *MyGameInstance = GetGameInstance();
    if (MyGameInstance)
    {
        if (WeaponSlots.IsValidIndex(IndexSlot))
        {
            MyGameInstance->GetWeaponInfoByName(WeaponSlots[IndexSlot].NameItem, OutInfo);
            WeaponType = OutInfo.WeaponType;
            bIsFind = true;
        }
    }
    return bIsFind;
}

int32 UTDSInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
    int32 Result = -1;
    bool bIsFind = false;
    int8 i = 0;
    while (i < WeaponSlots.Num() && !bIsFind)
    {
        if (WeaponSlots[i].NameItem == IdWeaponName)
        {
            Result = i;
            bIsFind = true;
        }
        i++;
    }
    return Result;
}

FName UTDSInventoryComponent::GetWeaponNameBySlotIndex(int32 IndexSlot)
{
    FName Result;
    if (WeaponSlots.IsValidIndex(IndexSlot))
    {
        Result = WeaponSlots[IndexSlot].NameItem;
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::GetWeaponNameBySlotIndex - Not Correct index Weapon  - %d"), IndexSlot);
    }
    return Result;
}

FAdditionalWeaponInfo UTDSInventoryComponent::GetAdditionalInfoWeapon(int32 IndexWeapon)
{
    FAdditionalWeaponInfo Result;
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
        bool bIsFind = false;
        int8 i = 0;
        while (i < WeaponSlots.Num() && !bIsFind)
        {
            if (i == IndexWeapon)
            {
                Result = WeaponSlots[i].AdditionalInfo;
                bIsFind = true;
            }
            i++;
        }
		if (!bIsFind)
		{
            UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::GetAdditionakInfoWeapon - No Found Weapon with index - %d"), IndexWeapon);
		}
	}
	else
	{
        UE_LOG(LogTemp, Warning,TEXT("UTDSInventoryComponent::GetAdditionakInfoWeapon - Not correct index Weapon - %d"), IndexWeapon);
	}

	return Result;
}

void UTDSInventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
        bool bIsFind = false;
        int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
                WeaponSlots[i].AdditionalInfo = NewInfo;
                bIsFind = true;

                OnWeaponAdditionalInfoChangeEvent_Multicast(IndexWeapon, NewInfo);
			}
            i++;
		}
		if (!bIsFind)
		{
            UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"), IndexWeapon);
		}
	}
	else
	{
        UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::SetAdditionalInfoWeapon - Not correct index Weapon - %d"), IndexWeapon);
	}
}

void UTDSInventoryComponent::OnSwitchWeaponEvent_OnServer_Implementation(FName WeaponIdName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
    OnSwitchWeapon.Broadcast(WeaponIdName, WeaponAdditionalInfo, NewCurrentIndexWeapon);
}

void UTDSInventoryComponent::OnWeaponAdditionalInfoChangeEvent_Multicast_Implementation(int32 IndexSlot, FAdditionalWeaponInfo AdditionalInfo)
{
    OnWeaponAdditionalInfoChange.Broadcast(IndexSlot, AdditionalInfo);
}

// -------------------------------------------------- Init Inventory -------------------------------------------
void UTDSInventoryComponent::InitInventory_OnServer_Implementation(const TArray<FWeaponSlot> &NewWeaponSlotsInfo, const TArray<FAmmoSlot> &NewAmmoSlotsInfo)
{
    WeaponSlots = NewWeaponSlotsInfo;
    AmmoSlots = NewAmmoSlotsInfo;

    if (WeaponSlots.IsValidIndex(0) && !WeaponSlots[0].NameItem.IsNone())
    {
        OnSwitchWeaponEvent_OnServer(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
    }
}

TArray<FWeaponSlot> UTDSInventoryComponent::GetWeaponSlots()
{
    return WeaponSlots;
}

TArray<FAmmoSlot> UTDSInventoryComponent::GetAmmoSlots()
{
    return AmmoSlots;
}

// -------------------------------------------------- Change Ammo ----------------------------------------------
void UTDSInventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo)
{
    bool bIsFind = false;
    int8 i = 0;
    while (i < AmmoSlots.Num() && !bIsFind)
    {
        if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
            AmmoSlots[i].Cout += CoutChangeAmmo;
            if (AmmoSlots[i].Cout > AmmoSlots[i].MaxCout)
            {
                AmmoSlots[i].Cout = AmmoSlots[i].MaxCout;
            }
            AmmoChangeEvent_Multicast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);
            bIsFind = true;
		}
		i++;
    }
}

bool UTDSInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int32 &AviableAmmoForWeapon)
{
    AviableAmmoForWeapon = 0;
    bool bIsFind = false;
    int8 i = 0;
    while (i < AmmoSlots.Num() && !bIsFind)
    {
        if (AmmoSlots[i].WeaponType == TypeWeapon)
        {
            bIsFind = true;
            AviableAmmoForWeapon = AmmoSlots[i].Cout;
            if (AviableAmmoForWeapon > 0)
            {
                return true;
            }
        }
        i++;
    }

    if (AviableAmmoForWeapon <= 0)
    {
        OnWeaponAmmoEmptyEvent_Multicast(TypeWeapon);
    }
    else
    {
        OnWeaponAmmoAviableEvent_Multicast(TypeWeapon);
    }

    return false;
}

void UTDSInventoryComponent::AmmoChangeEvent_Multicast_Implementation(EWeaponType TypeAmmo, int32 Cout)
{
    OnAmmoChange.Broadcast(TypeAmmo, Cout);
}

void UTDSInventoryComponent::OnWeaponAmmoEmptyEvent_Multicast_Implementation(EWeaponType WeaponType)
{
    OnWeaponAmmoEmpty.Broadcast(WeaponType);
}

void UTDSInventoryComponent::OnWeaponAmmoAviableEvent_Multicast_Implementation(EWeaponType WeaponType)
{
    OnWeaponAmmoAviable.Broadcast(WeaponType);
}

void UTDSInventoryComponent::OnWeaponNotHaveRoundEvent_Multicast_Implementation(int32 IndexSlotWeapon)
{
    OnWeaponNotHaveRound.Broadcast(IndexSlotWeapon);
}

void UTDSInventoryComponent::OnWeaponHaveRoundEvent_Multicast_Implementation(int32 IndexSlotWeapon)
{
    OnWeaponHaveRound.Broadcast(IndexSlotWeapon);
}

// -------------------------------------------------- PickUp Actors --------------------------------------------
bool UTDSInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
    bool bResult = false;
    int8 i = 0;
    while (i < AmmoSlots.Num() && !bResult)
    {
        if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Cout < AmmoSlots[i].MaxCout)
        {
            bResult = true;
        }
        i++;
    }

    return bResult;
}

bool UTDSInventoryComponent::CheckCanTakeWeapon(int32 &FreeSlot)
{
    bool bIsFreeSlot = false;
    int8 i = 0;
    while (i < AmmoSlots.Num() && !bIsFreeSlot)
    {
        if (WeaponSlots[i].NameItem.IsNone())
        {
            bIsFreeSlot = true;
            FreeSlot = i;
        }
        i++;
    }
    return bIsFreeSlot;
}

void UTDSInventoryComponent::TryGetWeaponToInventory_OnServer_Implementation(AActor *PicUpActor, FWeaponSlot NewWeapon)
{
    int32 IndexSlot = -1;
    if (CheckCanTakeWeapon(IndexSlot))
    {
        if (WeaponSlots.IsValidIndex(IndexSlot))
        {
            WeaponSlots[IndexSlot] = NewWeapon;
            OnUpdateWeaponSlotEvent_Multicast(IndexSlot, NewWeapon);

            if (PicUpActor)
            {
                PicUpActor->Destroy();
            }
        }
    }
}

bool UTDSInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeapon, FDropItem &DropItemInfo)
{
    bool bResult = false;
    if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo))
    {
        WeaponSlots[IndexSlot] = NewWeapon;
        SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon, -1, NewWeapon.AdditionalInfo, true);
        OnUpdateWeaponSlotEvent_Multicast(IndexSlot, NewWeapon);
        bResult = true;
    }
    return bResult;
}

bool UTDSInventoryComponent::GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem &DropItemInfo)
{
    bool bResult = false;
    FName DropItemName =  GetWeaponNameBySlotIndex(IndexSlot);
    UTDSGameInstance *MyGameInstance = GetGameInstance();

    if (MyGameInstance)
    {
        bResult = MyGameInstance->GetDropItemInfoByWeaponName(DropItemName, DropItemInfo);
        if (WeaponSlots.IsValidIndex(IndexSlot))
        {
            DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
        }
    }

    return bResult;
}

void UTDSInventoryComponent::OnUpdateWeaponSlotEvent_Multicast_Implementation(int32 IndexSlotChange, FWeaponSlot NewInfo)
{
    OnUpdateWeaponSlot.Broadcast(IndexSlotChange, NewInfo);
}

// -------------------------------------------------- Drop Item To World ---------------------------------------
void UTDSInventoryComponent::DropWeaponByIndex_OnServer_Implementation(int32 ByIndex)
{
    FDropItem DropItemInfo;
    FWeaponSlot EmtyWeaponSlot;

    bool bIsCanDrop = false;
    int8 i = 0;
    int8 AviableWeaponNum = 0;
    while (i < WeaponSlots.Num() && !bIsCanDrop)
    {
        if (!WeaponSlots[i].NameItem.IsNone())
        {
            AviableWeaponNum++;
            if (AviableWeaponNum > 1)
            {
                bIsCanDrop = true;
            }
        }
        i++;
    }

    if (bIsCanDrop && WeaponSlots.IsValidIndex(ByIndex) && GetDropItemInfoFromInventory(ByIndex, DropItemInfo))
    {
        bool bIsFindWeapon = false;
        int8 j = 0;
        while (j < WeaponSlots.Num() && !bIsFindWeapon)
        {
            if (!WeaponSlots[i].NameItem.IsNone())
            {
                OnSwitchWeaponEvent_OnServer(WeaponSlots[j].NameItem, WeaponSlots[j].AdditionalInfo, j);
            }
            j++;
        }

        WeaponSlots[ByIndex] = EmtyWeaponSlot;
        if (GetOwner()->GetClass()->ImplementsInterface(UTDS_IGameActor::StaticClass()))
        {
            ITDS_IGameActor::Execute_DropWeaponToWorld(GetOwner(), DropItemInfo);
         }

        OnUpdateWeaponSlotEvent_Multicast(ByIndex, EmtyWeaponSlot);
    }
}