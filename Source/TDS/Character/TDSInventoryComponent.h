#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FuncLibrary/Types.h"
#include "Game/TDSGameInstance.h"
#include "Interface/TDS_IGameActor.h"
#include "Net/UnrealNetwork.h"
#include "TDSInventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeapon, FName, WeaponIdName, FAdditionalWeaponInfo, WeaponAdditionalInfo, int32, NewCurrentIndexWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChange, int32, IndexSlot, FAdditionalWeaponInfo, AdditionalInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, EWeaponType, TypeAmmo, int32, Cout);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlot, int32, IndexSlotChange, FWeaponSlot, NewInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoEmpty, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoAviable, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponNotHaveRound, int32, IndexSlotWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponHaveRound, int32, IndexSlotWeapon);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_API UTDSInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTDSInventoryComponent();

// -------------------------------------------------- For Game -------------------------------------------------
    UTDSGameInstance* GetGameInstance();

// -------------------------------------------------- Multiplayer --------------------------------------------
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const override;

// -------------------------------------------------- Switch Weapon --------------------------------------------
    UPROPERTY(BlueprintAssignable, Category = "Inventory") 
    FOnSwitchWeapon OnSwitchWeapon;
    UPROPERTY(BlueprintAssignable, Category = "Inventory")
    FOnWeaponAdditionalInfoChange OnWeaponAdditionalInfoChange;
    UPROPERTY(BlueprintAssignable, Category = "Inventory")
    FOnAmmoChange OnAmmoChange;
    UPROPERTY(BlueprintAssignable, Category = "Inventory")
    FOnUpdateWeaponSlot OnUpdateWeaponSlot;
    UPROPERTY(BlueprintAssignable, Category = "Inventory")
    FOnWeaponAmmoEmpty OnWeaponAmmoEmpty;
    UPROPERTY(BlueprintAssignable, Category = "Inventory")
    FOnWeaponAmmoAviable OnWeaponAmmoAviable;
    UPROPERTY(BlueprintAssignable, Category = "Inventory")
    FOnWeaponNotHaveRound OnWeaponNotHaveRound;
    UPROPERTY(BlueprintAssignable, Category = "Inventory")
    FOnWeaponHaveRound OnWeaponHaveRound;

    bool SwitchWeaponToIndexByNextPreviosIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward);
    bool SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviosIndex, FAdditionalWeaponInfo PreviosWeaponInfo);
    bool GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType &WeaponType);
    bool GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType &WeaponType);
    int32 GetWeaponIndexSlotByName(FName IdWeaponName);
    FName GetWeaponNameBySlotIndex(int32 IndexSlot);
    FAdditionalWeaponInfo GetAdditionalInfoWeapon(int32 IndexWeapon);
    void SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo);
    UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Delegate")
    void OnSwitchWeaponEvent_OnServer(FName WeaponIdName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Delegate")
    void OnWeaponAdditionalInfoChangeEvent_Multicast(int32 IndexSlot, FAdditionalWeaponInfo AdditionalInfo);
    

// -------------------------------------------------- Init Inventory -------------------------------------------
    UPROPERTY(Replicated, BlueprintReadOnly, Category = "Weapons")
    TArray<FWeaponSlot> WeaponSlots;
    UPROPERTY(Replicated, BlueprintReadOnly, Category = "Weapons")
    TArray<FAmmoSlot> AmmoSlots;

    UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
    void InitInventory_OnServer(const TArray<FWeaponSlot>& NewWeaponSlotsInfo, const TArray<FAmmoSlot>& NewAmmoSlotsInfo);
    UFUNCTION(BlueprintCallable, Category = "Inventory")
    TArray<FWeaponSlot> GetWeaponSlots();
    UFUNCTION(BlueprintCallable, Category = "Inventory")
    TArray<FAmmoSlot> GetAmmoSlots();

// -------------------------------------------------- Change Ammo ----------------------------------------------
    UFUNCTION(BlueprintCallable)
    void AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo);
    bool CheckAmmoForWeapon(EWeaponType TypeWeapon, int32 &AviableAmmoForWeapon);
    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Delegate")
    void AmmoChangeEvent_Multicast(EWeaponType TypeAmmo, int32 Cout);
    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Delegate")
    void OnWeaponAmmoEmptyEvent_Multicast(EWeaponType WeaponType);
    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Delegate")
    void OnWeaponAmmoAviableEvent_Multicast(EWeaponType WeaponType);
    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Delegate")
    void OnWeaponNotHaveRoundEvent_Multicast(int32 IndexSlotWeapon);
    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Delegate")
    void OnWeaponHaveRoundEvent_Multicast(int32 IndexSlotWeapon);

// -------------------------------------------------- PickUp Actors --------------------------------------------
    UFUNCTION(BlueprintCallable, Category = "Interface")
    bool CheckCanTakeAmmo(EWeaponType AmmoType);
    UFUNCTION(BlueprintCallable, Category = "Interface")
    bool CheckCanTakeWeapon(int32 &FreeSlot);
    UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
    void TryGetWeaponToInventory_OnServer(AActor* PicUpActor, FWeaponSlot NewWeapon);
    UFUNCTION(BlueprintCallable, Category = "Interface")
    bool SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeapon, FDropItem &DropItemInfo);
    UFUNCTION(BlueprintCallable, Category = "Interface")
    bool GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem &DropItemInfo);
    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Delegate")
    void OnUpdateWeaponSlotEvent_Multicast(int32 IndexSlotChange, FWeaponSlot NewInfo);

// -------------------------------------------------- Drop Item To World ---------------------------------------
    UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
    void DropWeaponByIndex_OnServer(int32 ByIndex);

  protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};