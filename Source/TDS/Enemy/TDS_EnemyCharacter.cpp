#include "Enemy/TDS_EnemyCharacter.h"

ATDS_EnemyCharacter::ATDS_EnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    // Network
    bReplicates = true;
}

void ATDS_EnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATDS_EnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATDS_EnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

// -------------------------------------------------- For Replicate --------------------------------------------
void ATDS_EnemyCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ATDS_EnemyCharacter, UsedEffects);
    DOREPLIFETIME(ATDS_EnemyCharacter, EffectAdd);
    DOREPLIFETIME(ATDS_EnemyCharacter, EffectRemove);
}

// -------------------------------------------------- State Effect -----------------------------------------------
EPhysicalSurface ATDS_EnemyCharacter::GetSurfaceType()
{
    EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
    UStaticMeshComponent *MyMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
    if (MyMesh)
    {
        UMaterialInterface *MyMaterial = MyMesh->GetMaterial(0);
        if (MyMaterial)
        {
            Result = MyMaterial->GetPhysicalMaterial()->SurfaceType;
        }
    }
    return Result;
}

void ATDS_EnemyCharacter::AddEffect_Implementation(UTDS_StateEffect *NewEffect)
{
    UsedEffects.Add(NewEffect);

    if (!NewEffect->bIsAutoDestroyParticleEffect)
    {
        SwitchEffect(NewEffect, true);
        EffectAdd = NewEffect;
    }
    else
    {
        if (NewEffect->ParticleEffect)
        {
            ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
        }
    }
}

void ATDS_EnemyCharacter::RemoveEffect_Implementation(UTDS_StateEffect *RemoveEffect)
{
    UsedEffects.Remove(RemoveEffect);

    if (!RemoveEffect->bIsAutoDestroyParticleEffect)
    {
        SwitchEffect(RemoveEffect, false);
        EffectRemove = RemoveEffect;
    }
}

void ATDS_EnemyCharacter::EffectAdd_OnRep()
{
    if (EffectAdd)
    {
        SwitchEffect(EffectAdd, true);
    }
}

void ATDS_EnemyCharacter::EffectRemove_OnRep()
{
    if (EffectRemove)
    {
        SwitchEffect(EffectRemove, false);
    }
}

void ATDS_EnemyCharacter::SwitchEffect(UTDS_StateEffect *Effect, bool bIsAdd)
{
    if (bIsAdd)
    {
        if (Effect && Effect->ParticleEffect)
        {
            FName NameBoneToAttached = Effect->NameBone;
            FVector LocationEffect = FVector(0);

            USkeletalMeshComponent *MySkeletalMesh = GetMesh();
            if (MySkeletalMesh)
            {
                UParticleSystemComponent *NewParticleSystem = UGameplayStatics::SpawnEmitterAttached(
                    Effect->ParticleEffect, MySkeletalMesh, NameBoneToAttached, LocationEffect, FRotator::ZeroRotator,
                    EAttachLocation::SnapToTarget, false);
                ParticleSystemEffects.Add(NewParticleSystem);
            }
        }
    }
    else
    {
        int32 i = 0;
        bool bIsFind = false;
        if (ParticleSystemEffects.Num() > 0)
        {
            while (i < ParticleSystemEffects.Num() && !bIsFind)
            {
                if (ParticleSystemEffects[i]->Template && Effect->ParticleEffect &&
                    Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
                {
                    bIsFind = true;
                    ParticleSystemEffects[i]->DeactivateSystem();
                    ParticleSystemEffects[i]->DestroyComponent();
                    ParticleSystemEffects.RemoveAt(i);
                }
            }
        }
    }
}

void ATDS_EnemyCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem *ExecuteFX)
{
    ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATDS_EnemyCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem *ExecuteFX)
{
    UTypes::ExecuteEffectAdded(ExecuteFX, this, FVector(0), FName("Spine_01"));
}

bool ATDS_EnemyCharacter::ReplicateSubobjects(UActorChannel *Channel, FOutBunch *Bunch, FReplicationFlags *RepFlags)
{
    bool bWrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

    for (int32 i = 0; i < UsedEffects.Num(); i++)
    {
        if (UsedEffects[i])
        {
            bWrote |= Channel->ReplicateSubobject(UsedEffects[i], *Bunch, *RepFlags);
        }
    }
    return bWrote;
}