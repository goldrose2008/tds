#include "FuncLibrary/Types.h"
#include "Interface/TDS_IGameActor.h"
#include "TDS.h"

// -------------------------------------------------- StateEffect -------------------------------------------------
void UTypes::AddEffectBySurfaceType(AActor *TakeEffectActor, FName NameBoneHit, TSubclassOf<UTDS_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
    if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
    {
        UTDS_StateEffect *MyEffect = Cast<UTDS_StateEffect>(AddEffectClass->GetDefaultObject());
        if (MyEffect)
        {
            bool bIsHavePossibleSurface = false;
            int8 i = 0;
            while (i < MyEffect->PossibleInteractSurface.Num() && !bIsHavePossibleSurface)
            {
                if (MyEffect->PossibleInteractSurface[i] == SurfaceType)
                {
                    bIsHavePossibleSurface = true;
                    bool bIsCanAddEffect = false;
                    if (!MyEffect->bIsStakable)
                    {
                        TArray<UTDS_StateEffect*> CurrentEffects;
                        ITDS_IGameActor* MyInterface = Cast<ITDS_IGameActor>(TakeEffectActor);
                        if (MyInterface)
                        {
                            CurrentEffects = MyInterface->GetAllCurrentEffects();
                        }
                        if (CurrentEffects.Num() > 0)
                        {
                            int8 j = 0;
                            while (j < CurrentEffects.Num() && !bIsCanAddEffect)
                            {
                                if (CurrentEffects[j]->GetClass() != AddEffectClass)
                                {
                                    bIsCanAddEffect = true;
                                }
                                j++;
                            }
                        }
                        else
                        {
                            bIsCanAddEffect = true;
                        }
                    }
                    else
                    {
                        bIsCanAddEffect = true;
                    }

                    if (bIsCanAddEffect)
                    {
                        UTDS_StateEffect *NewEffect = NewObject<UTDS_StateEffect>(TakeEffectActor, AddEffectClass);
                        if (NewEffect)
                        {
                            NewEffect->InitObject(TakeEffectActor, NameBoneHit);
                        }
                    }
                }
                i++;
            }
        }
    }
}

void UTypes::ExecuteEffectAdded(UParticleSystem *ExecuteFX, AActor *Target, FVector Offset, FName Socket)
{
    if (Target)
    {
        FName SocketToAttached = Socket;
        FVector Location = Offset;
        ACharacter *MyCharacter = Cast<ACharacter>(Target);
        if (MyCharacter && MyCharacter->GetMesh())
        {
            UGameplayStatics::SpawnEmitterAttached(ExecuteFX, MyCharacter->GetMesh(), SocketToAttached, Location, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
        }
        else
        {
            if (Target->GetRootComponent())
            {
                UGameplayStatics::SpawnEmitterAttached(ExecuteFX, Target->GetRootComponent(), SocketToAttached, Location, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
            }
        }
    }
}
