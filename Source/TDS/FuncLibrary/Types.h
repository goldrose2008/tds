#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "StateEffect/TDS_StateEffect.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "Types.generated.h"

// -------------- Movement --------------
UENUM(BlueprintType)
enum class EMovementState : uint8
{
    Aim_State UMETA(DisplayName = "Aim State"),
    AimWalk_State UMETA(DisplayName = "AimWalk State"),
    Walk_State UMETA(DisplayName = "Walk State"),
    Run_State UMETA(DisplayName = "Run State"),
    SprintRun_State UMETA(DisplayName = "SprintRun State")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
        float AimSpeedNormal = 300.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
        float WalkSpeedNormal = 200.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
        float RunSpeedNormal = 600.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
        float AimWalkSpeed = 100.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
        float SprintRunSpeed = 800.0f;
};

// -------------- Projectile ------------
USTRUCT(BlueprintType)
struct FProjectileInfo
{
        GENERATED_USTRUCT_BODY()

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
        TSubclassOf<class AProjectileDefault> Projectile = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
        UStaticMesh* ProjectileStaticMesh = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
        FTransform ProjectileStaticMeshOffset = FTransform();
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
        UParticleSystem *ProjectileTrailFX = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
        FTransform ProjectileTrailFxOffset = FTransform();

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
        float ProjectileDamage = 20.0f;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
        float ProjectileLifeTime = 20.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
        float ProjectileInitSpeed = 2000.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
        float ProjectileMaxSpeed = 2000.0f;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
        TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface *> HitDecals;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
        TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem *> HitFXs;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
        USoundBase *HitSound = nullptr;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
        TSubclassOf<UTDS_StateEffect> StateEffect = nullptr;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Expode")
        float ProjectileMinRadiusDamage = 100.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Expode")
        float ProjectileMaxRadiusDamage = 200.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Expode")
        float ExplodeMaxDamage = 40.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Expode")
        UParticleSystem *ExplodeFX = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Expode")
        USoundBase *ExplodeSound = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Expode")
        float ExplodeFalloffCoef = 1.0f;
};

USTRUCT(BlueprintType)
struct FDropMeshInfo
{
        GENERATED_USTRUCT_BODY()

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
        UStaticMesh *DropMesh = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
        float DropMeshTime = -1.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
        float DropMeshLifeTime = 5.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
        FTransform DropMeshOffset = FTransform();
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
        FVector DropMeshImpulseDirection = FVector(0.0f);
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
        float PowerImpulse = 0.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
        float ImpulseRandomDispersion = 0.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
        float CustomMass = 0.0f;
};

// -------------- Animation ------------
USTRUCT(BlueprintType)
struct FAnimationWeaponInfo
{
        GENERATED_USTRUCT_BODY()

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Character")
        UAnimMontage *AnimCharFire = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Character")
        UAnimMontage *AnimCharFireAim = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Character")
        UAnimMontage *AnimCharReload = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Character")
        UAnimMontage *AnimCharReloadAim = nullptr;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Weapon")
        UAnimMontage *AnimWeaponReload = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Weapon")
        UAnimMontage *AnimWeaponReloadAim = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Weapon")
        UAnimMontage *AnimWeaponFire = nullptr;
};

// --------------For Weapon ----------------
USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
        GENERATED_USTRUCT_BODY()

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Stats")
        int32 Round = 10;
};

// -------------- Inventory ----------------
UENUM(BlueprintType)
enum class EWeaponType : uint8
{
        RifleType UMETA(DisplayName = "Rifle"),
        GrenadeLauncher UMETA(DisplayName = "GrenadeLauncher"),
        ShotGunType UMETA(DisplayName = "ShotGu")
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
        GENERATED_USTRUCT_BODY()

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
        FName NameItem;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
        FAdditionalWeaponInfo AdditionalInfo;
};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
        GENERATED_USTRUCT_BODY()

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
        EWeaponType WeaponType = EWeaponType::RifleType;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
        int32 Cout = 100;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
        int32 MaxCout = 100;
};

// -------------- PickUp Weapon ----------------
USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase
{
        GENERATED_USTRUCT_BODY()

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
        UStaticMesh* WeaponStaticMesh = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
        USkeletalMesh *WeaponSkeletalMesh = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
        UParticleSystem *ParticleItem = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
        FTransform Offset;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
        FWeaponSlot WeaponInfo;
};

// -------------- Weapon ----------------
USTRUCT(BlueprintType)
struct FWeaponDispersion
{
        GENERATED_USTRUCT_BODY()

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Aim_StateDispersionAimMax = 2.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Aim_StateDispersionAimMin = 0.3f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Aim_StateDispersionAimRecoil = 1.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Aim_StateDispersionReduction = 0.3f;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float AimWalk_StateDispersionAimMax = 1.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float AimWalk_StateDispersionAimMin = 0.1f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float AimWalk_StateDispersionAimRecoil = 1.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float AimWalk_StateDispersionReduction = 0.4f;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Walk_StateDispersionAimMax = 5.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Walk_StateDispersionAimMin = 1.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Walk_StateDispersionAimRecoil = 1.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Walk_StateDispersionReduction = 0.2f;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Run_StateDispersionAimMax = 10.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Run_StateDispersionAimMin = 4.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Run_StateDispersionAimRecoil = 1.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Run_StateDispersionReduction = 0.1f;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
        GENERATED_USTRUCT_BODY()

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
        TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
        float RateOfFire = 0.5f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
        float ReloadTime = 2.0f;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
        int32 MaxRound = 10;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
        int32 NumberProjectileByShot = 1;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        FWeaponDispersion DispersionWeapon;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
        USoundBase *SoundFireWeapon = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
        USoundBase *SoundReloadWeapon = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
        UParticleSystem *EffectFireWeapon = nullptr;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
        FProjectileInfo ProjectileSetting;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
        float DistanceTrace = 2000.0f;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitEffect")
        UDecalComponent *DecalOnHit = nullptr;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
        FAnimationWeaponInfo AnimWeaponInfo;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
        FDropMeshInfo ClipDropMesh;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
        FDropMeshInfo ShellBullets;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
        float SwitchTimeToWeapon = 1.0f;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
        UTexture2D* WeaponIcon = nullptr;
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
        EWeaponType WeaponType = EWeaponType::RifleType;
};

UCLASS()
class TDS_API UTypes : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

public:
// -------------------------------------------------- StateEffect -------------------------------------------------
    UFUNCTION(BlueprintCallable)
    static void AddEffectBySurfaceType(AActor *TakeEffectActor, FName NameBoneHit, TSubclassOf<UTDS_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType);

    UFUNCTION(BlueprintCallable)
    static void ExecuteEffectAdded(UParticleSystem *ExecuteFX, AActor *Target, FVector Offset, FName Socket);
};