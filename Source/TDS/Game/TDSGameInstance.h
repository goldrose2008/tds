#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "FuncLibrary/Types.h"
#include "Engine/DataTable.h"
#include "Weapon/WeaponDefault.h"
#include "TDSGameInstance.generated.h"

UCLASS()
class TDS_API UTDSGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
// -------------------------------------------------- Table WeaponInfo ----------------------------------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
	UDataTable *WeaponInfoTable = nullptr;

	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo &OutInfo);

// -------------------------------------------------- Table DropItemInfo -------------------------------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UDataTable *DropItemInfoTable = nullptr;

	UFUNCTION(BlueprintCallable)
	bool GetDropItemInfoByWeaponName(FName NameItem, FDropItem &OutInfo);
    UFUNCTION(BlueprintCallable)
    bool GetDropItemInfoByName(FName NameItem, FDropItem &OutInfo);

// -------------------------------------------------- For Option Widget // -------------------------------------------
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "OptionAudio")
    float CurrentValueEffect = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "OptionAudio")
    float CurrentValueMusic = 1.0f;
};