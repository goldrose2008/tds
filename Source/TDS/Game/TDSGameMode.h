// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "StateEffect/TDS_StateEffect_Electric.h"
#include "Engine/World.h"
#include "TDSGameMode.generated.h"

UCLASS(minimalapi)
class ATDSGameMode : public AGameModeBase
{
    GENERATED_BODY()

public:
    ATDSGameMode();
};