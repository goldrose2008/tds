#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "StateEffect/TDS_StateEffect.h"
#include "FuncLibrary/Types.h"
#include "TDS_IGameActor.generated.h"

UINTERFACE(MinimalAPI)
class UTDS_IGameActor : public UInterface
{
	GENERATED_BODY()
};

class TDS_API ITDS_IGameActor
{
	GENERATED_BODY()

public:
// -------------------------------------------------- State Effect -----------------------------------------------
	virtual EPhysicalSurface GetSurfaceType();
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    void AddEffect(UTDS_StateEffect *NewEffect);
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    void RemoveEffect(UTDS_StateEffect *RemoveEffect);
	virtual TArray<UTDS_StateEffect*> GetAllCurrentEffects();

// -------------------------------------------------- Inventory --------------------------------------------------
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    void DropWeaponToWorld(FDropItem DropItemInfo);
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Cout);
};
