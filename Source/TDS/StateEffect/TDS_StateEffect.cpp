#include "StateEffect/TDS_StateEffect.h"
#include "TDSCharacter.h"
#include "Interface/TDS_IGameActor.h"

// -------------------------------------------------- For Replicate --------------------------------------------
void UTDS_StateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UTDS_StateEffect, NameBone);
}
// -------------------------------------------------- UTDS_StateEffect --------------------------------------------------
bool UTDS_StateEffect::InitObject(AActor *Actor, FName NameBoneHit)
{
    MyActor = Actor;
    NameBone = NameBoneHit;

    ITDS_IGameActor* MyInterface = Cast<ITDS_IGameActor>(MyActor);
    if (MyInterface)
    {
        MyInterface->Execute_AddEffect(MyActor, this);
    }

    return true;
}

void UTDS_StateEffect::DestroyObject()
{
    ITDS_IGameActor *MyInterface = Cast<ITDS_IGameActor>(MyActor);
    if (MyInterface)
    {
        MyInterface->Execute_RemoveEffect(MyActor, this);
    }

    MyActor = nullptr;

    if (this && this->IsValidLowLevel())
    {
        this->ConditionalBeginDestroy();
    }
}

// -------------------------------------------------- UTDS_StateEffect_ExecuteOnce --------------------------------------------------
bool UTDS_StateEffect_ExecuteOnce::InitObject(AActor *Actor, FName NameBoneHit)
{
    Super::InitObject(Actor, NameBoneHit);
    ExecuteOnce();
    return true;
}

void UTDS_StateEffect_ExecuteOnce::DestroyObject()
{
    Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteOnce::ExecuteOnce()
{
    if (MyActor)
    {
        UTDSHealthComponent *MyHealthComponent = Cast<UTDSHealthComponent>( MyActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
        if (MyHealthComponent)
        {
            MyHealthComponent->ChangeCurrentHealth_OnServer(Power);
        }
    }

    DestroyObject();
}

// -------------------------------------------------- UTDS_StateEffect_ExecuteTimer --------------------------------------------------
bool UTDS_StateEffect_ExecuteTimer::InitObject(AActor *Actor, FName NameBoneHit)
{
    Super::InitObject(Actor, NameBoneHit);
    if (GetWorld())
    {
        GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
        GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_StateEffect_ExecuteTimer::Execute, RateTime, true);
    }
    return true;
}

void UTDS_StateEffect_ExecuteTimer::DestroyObject()
{
    if (GetWorld())
    {
        GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
    }

    Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteTimer::Execute()
{
    if (MyActor)
    {
        UTDSHealthComponent *MyHealthComponent = Cast<UTDSHealthComponent>(MyActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
        if (MyHealthComponent)
        {
            MyHealthComponent->ChangeCurrentHealth_OnServer(Power);
        }
    }
}

// -------------------------------------------------- UTDS_StateEffect_ExecuteLong --------------------------------------------------
bool UTDS_StateEffect_ExecuteLong::InitObject(AActor *Actor, FName NameBoneHit)
{
    Super::InitObject(Actor, NameBoneHit);

    if (GetWorld())
    {
        GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEffect_ExecuteLong::ExecuteLong, RateTime, true);
    }

    return true;
}

void UTDS_StateEffect_ExecuteLong::DestroyObject()
{
    if (GetWorld())
    {
        GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
    }

    Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteLong::ExecuteLong()
{
    ATDSCharacter *MyCharacter = Cast<ATDSCharacter>(MyActor);
    UTDSHealthComponent *MyHealthComponent = Cast<UTDSHealthComponent>(MyCharacter->GetComponentByClass(UTDSHealthComponent::StaticClass()));

    if (MyCharacter && MyHealthComponent)
    {
        if (MyCharacter->bOverlap_Electric)
        {
            MyHealthComponent->ChangeCurrentHealth_OnServer(Power);
        }
        else
        {
            DestroyObject();
        }
    }
}