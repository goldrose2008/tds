#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Character/TDSHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Net/UnrealNetwork.h"
#include "TDS_StateEffect.generated.h"

UCLASS(Blueprintable, BlueprintType)
class TDS_API UTDS_StateEffect : public UObject
{
	GENERATED_BODY()

public:
// -------------------------------------------------- For Replicate --------------------------------------------
    virtual bool IsSupportedForNetworking() const override {return true;};
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const override;

//-------------------------------------------------------------------
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
    TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
    bool bIsStakable = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
    UParticleSystem *ParticleEffect = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
    bool bIsAutoDestroyParticleEffect = false;
    AActor *MyActor = nullptr;
    UPROPERTY(Replicated)
    FName NameBone;

    virtual bool InitObject(AActor *Actor, FName NameBoneHit);
    virtual void DestroyObject();
};

UCLASS()
class TDS_API UTDS_StateEffect_ExecuteOnce : public UTDS_StateEffect
{
    GENERATED_BODY()

public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
    float Power = 20.0f;

    bool InitObject(AActor *Actor, FName NameBoneHit) override;
    void DestroyObject() override;
    virtual void ExecuteOnce();
};

UCLASS()
class TDS_API UTDS_StateEffect_ExecuteTimer : public UTDS_StateEffect
{
    GENERATED_BODY()

  public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
    float Power = 20.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
    float Timer = 5.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
    float RateTime = 1.0f;
    FTimerHandle TimerHandle_ExecuteTimer;
    FTimerHandle TimerHandle_EffectTimer;

    bool InitObject(AActor *Actor, FName NameBoneHit) override;
    void DestroyObject() override;
    virtual void Execute();
};

// -------------------------------------------------- UTDS_StateEffect_ExecuteLong --------------------------------------------------
UCLASS()
class TDS_API UTDS_StateEffect_ExecuteLong : public UTDS_StateEffect
{
    GENERATED_BODY()

  public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Long")
    float Power = 20.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Long")
    float RateTime = 1.0f;
    FTimerHandle TimerHandle_EffectTimer;

    bool InitObject(AActor *Actor, FName NameBoneHit) override;
    void DestroyObject() override;
    void ExecuteLong();
};
