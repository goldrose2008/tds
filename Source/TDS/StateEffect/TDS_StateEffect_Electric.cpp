#include "StateEffect/TDS_StateEffect_Electric.h"

ATDS_StateEffect_Electric::ATDS_StateEffect_Electric()
{
	PrimaryActorTick.bCanEverTick = true;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
    CollisionSphere->SetSphereRadius(150.0f);
    CollisionSphere->bReturnMaterialOnMove = true;            // hit event return physMaterial
    CollisionSphere->SetCanEverAffectNavigation(false);       // collision not affect navigation (P keybord on editor)

    RootComponent = CollisionSphere;

    ActorFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Actor FX"));
    ActorFX->SetupAttachment(RootComponent);
}

void ATDS_StateEffect_Electric::BeginPlay()
{
	Super::BeginPlay();

	OnActorBeginOverlap.AddDynamic(this, &ATDS_StateEffect_Electric::ActorBeginOverlap_Electric);
    OnActorEndOverlap.AddDynamic(this, &ATDS_StateEffect_Electric::ActorEndOverlap_Electric);

}

void ATDS_StateEffect_Electric::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATDS_StateEffect_Electric::ActorBeginOverlap_Electric(AActor *OverlappedActor, AActor *OtherActor)
{
    ATDSCharacter *MyCharacter = Cast<ATDSCharacter>(OtherActor);
	if (MyCharacter)
	{
        MyCharacter->bOverlap_Electric = true;
        UTypes::AddEffectBySurfaceType(OtherActor, NAME_None, StateEffect_Electric, EPhysicalSurface::SurfaceType5);
	}

}

void ATDS_StateEffect_Electric::ActorEndOverlap_Electric(AActor *OverlappedActor, AActor *OtherActor)
{
    ATDSCharacter *MyCharacter = Cast<ATDSCharacter>(OtherActor);
    if (MyCharacter)
    {
        MyCharacter->bOverlap_Electric = false;
    }
}
