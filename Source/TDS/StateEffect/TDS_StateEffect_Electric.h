#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/TDS_IGameActor.h"
#include "FuncLibrary/Types.h"
#include "TDSCharacter.h"
#include "TDS_StateEffect_Electric.generated.h"

UCLASS()
class TDS_API ATDS_StateEffect_Electric : public AActor
{
	GENERATED_BODY()
	
public:	
	ATDS_StateEffect_Electric();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USphereComponent *CollisionSphere = nullptr;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class UParticleSystemComponent *ActorFX = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StateEffect")
    TSubclassOf<UTDS_StateEffect_ExecuteLong> StateEffect_Electric;
    UTDS_StateEffect_ExecuteLong *MyEffect_Electric = nullptr;

	UFUNCTION()
	void ActorBeginOverlap_Electric(AActor *OverlappedActor, AActor *OtherActor);
    UFUNCTION()
    void ActorEndOverlap_Electric(AActor *OverlappedActor, AActor *OtherActor);
};