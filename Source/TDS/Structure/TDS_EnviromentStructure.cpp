#include "Structure/TDS_EnviromentStructure.h"

ATDS_EnviromentStructure::ATDS_EnviromentStructure()
{
	PrimaryActorTick.bCanEverTick = true;

    // Network
    bReplicates = true;
}

void ATDS_EnviromentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATDS_EnviromentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// -------------------------------------------------- For Replicate --------------------------------------------
void ATDS_EnviromentStructure::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ATDS_EnviromentStructure, UsedEffects);
    DOREPLIFETIME(ATDS_EnviromentStructure, EffectAdd);
    DOREPLIFETIME(ATDS_EnviromentStructure, EffectRemove);
}

// -------------------------------------------------- State Effect -----------------------------------------------
EPhysicalSurface ATDS_EnviromentStructure::GetSurfaceType()
{
    EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
    UStaticMeshComponent *MyMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (MyMesh)
	{
        UMaterialInterface* MyMaterial = MyMesh->GetMaterial(0);
		if (MyMaterial)
		{
            Result = MyMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
    return Result;
}

void ATDS_EnviromentStructure::AddEffect_Implementation(UTDS_StateEffect *NewEffect)
{
    UsedEffects.Add(NewEffect);

    if (!NewEffect->bIsAutoDestroyParticleEffect)
    {
        SwitchEffect(NewEffect, true);
        EffectAdd = NewEffect;
    }
    else
    {
        if (NewEffect->ParticleEffect)
        {
            ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
        }
    }
}

void ATDS_EnviromentStructure::RemoveEffect_Implementation(UTDS_StateEffect *RemoveEffect)
{
    UsedEffects.Remove(RemoveEffect);

    if (!RemoveEffect->bIsAutoDestroyParticleEffect)
    {
        SwitchEffect(RemoveEffect, false);
        EffectRemove = RemoveEffect;
    }
}

void ATDS_EnviromentStructure::EffectAdd_OnRep()
{
    if (EffectAdd)
    {
        SwitchEffect(EffectAdd, true);
    }
}

void ATDS_EnviromentStructure::EffectRemove_OnRep()
{
    if (EffectRemove)
    {
        SwitchEffect(EffectRemove, false);
    }
}

void ATDS_EnviromentStructure::SwitchEffect(UTDS_StateEffect* Effect, bool bIsAdd)
{
    if (bIsAdd)
    {
        if (Effect && Effect->ParticleEffect)
        {
            FName NameBoneToAttached = NAME_None;
            FVector LocationEffect = FVector(0);

            USceneComponent *MySceneComponent = GetRootComponent();
            if (MySceneComponent)
            {
                UParticleSystemComponent *NewParticleSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, MySceneComponent, NameBoneToAttached, LocationEffect, FRotator::ZeroRotator,
                                                              EAttachLocation::SnapToTarget, false);
                ParticleSystemEffects.Add(NewParticleSystem);
            }
        }
    }
    else
    {
        int32 i = 0;
        bool bIsFind = false;
        if (ParticleSystemEffects.Num() > 0)
        {
            while (i < ParticleSystemEffects.Num() && !bIsFind)
            {
                if (ParticleSystemEffects[i]->Template && Effect->ParticleEffect && Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
                {
                    bIsFind = true;
                    ParticleSystemEffects[i]->DeactivateSystem();
                    ParticleSystemEffects[i]->DestroyComponent();
                    ParticleSystemEffects.RemoveAt(i);
                }
            }
        }
    }
}

void ATDS_EnviromentStructure::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem *ExecuteFX)
{
    ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATDS_EnviromentStructure::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem *ExecuteFX)
{
    UTypes::ExecuteEffectAdded(ExecuteFX, this, FVector(0), NAME_None);
}

bool ATDS_EnviromentStructure::ReplicateSubobjects(UActorChannel *Channel, FOutBunch *Bunch, FReplicationFlags *RepFlags)
{
    bool bWrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

    for (int32 i = 0; i < UsedEffects.Num(); i++)
    {
        if (UsedEffects[i])
        {
            bWrote |= Channel->ReplicateSubobject(UsedEffects[i], *Bunch, *RepFlags);
        }
    }
    return bWrote;
}

TArray<UTDS_StateEffect *> ATDS_EnviromentStructure::GetAllCurrentEffects()
{
    return UsedEffects;
}