#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/TDS_IGameActor.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "StateEffect/TDS_StateEffect.h"
#include "Net/UnrealNetwork.h"
#include "Engine/ActorChannel.h"
#include "TDS_EnviromentStructure.generated.h"

UCLASS()
class TDS_API ATDS_EnviromentStructure : public AActor, public ITDS_IGameActor
{
    GENERATED_BODY()

  public:
    ATDS_EnviromentStructure();

  protected:
    virtual void BeginPlay() override;

  public:
    virtual void Tick(float DeltaTime) override;

// -------------------------------------------------- For Replicate --------------------------------------------
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const override;

// -------------------------------------------------- State Effect -----------------------------------------------
    UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "StateEffects")
    TArray<UTDS_StateEffect *> UsedEffects;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
    TArray<UParticleSystemComponent *> ParticleSystemEffects;
    UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
    UTDS_StateEffect *EffectAdd = nullptr;
    UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
    UTDS_StateEffect *EffectRemove = nullptr;

    EPhysicalSurface GetSurfaceType() override;
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    void AddEffect(UTDS_StateEffect *NewEffect);
    void AddEffect_Implementation(UTDS_StateEffect *NewEffect) override;
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    void RemoveEffect(UTDS_StateEffect *RemoveEffect);
    void RemoveEffect_Implementation(UTDS_StateEffect *RemoveEffect) override;
    UFUNCTION()
    void EffectAdd_OnRep();
    UFUNCTION()
    void EffectRemove_OnRep();
    UFUNCTION()
    void SwitchEffect(UTDS_StateEffect *Effect, bool bIsAdd);
    UFUNCTION(Server, Reliable)
    void ExecuteEffectAdded_OnServer(UParticleSystem *ExecuteFX);
    UFUNCTION(NetMulticast, Reliable)
    void ExecuteEffectAdded_Multicast(UParticleSystem *ExecuteFX);
    bool ReplicateSubobjects(UActorChannel *Channel, FOutBunch *Bunch, FReplicationFlags *RepFlags) override;
    TArray<UTDS_StateEffect *> GetAllCurrentEffects() override;

    void TryAbilityEnabled();
};
