using UnrealBuildTool;

public class TDS : ModuleRules
{
    public TDS(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "PhysicsCore", "Slate" });

        PublicIncludePaths.AddRange(new string[] {
            "TDS",
            "TDS/Character",
            "TDS/FuncLibrary",
            "TDS/Game",
            "TDS/Interface",
            "TDS/StateEffect",
            "TDS/Structure",
            "TDS/Weapon"
        });
    }

}
