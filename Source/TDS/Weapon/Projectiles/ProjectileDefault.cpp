#include "Weapon/Projectiles/ProjectileDefault.h"

AProjectileDefault::AProjectileDefault()
{
	PrimaryActorTick.bCanEverTick = true;

    // Network
    bReplicates = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
    BulletCollisionSphere->SetSphereRadius(16.0f);
    BulletCollisionSphere->bReturnMaterialOnMove = true;      // hit event return physMaterial
    BulletCollisionSphere->SetCanEverAffectNavigation(false); // collision not affect navigation (P keybord on editor)

	RootComponent = BulletCollisionSphere;

    BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
    BulletMesh->SetupAttachment(RootComponent);
    BulletMesh->SetCanEverAffectNavigation(false);

    BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
    BulletFX->SetupAttachment(RootComponent);

    BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
    BulletProjectileMovement->UpdatedComponent = RootComponent;
    BulletProjectileMovement->bRotationFollowsVelocity = true;
    BulletProjectileMovement->bShouldBounce = false;
}

void AProjectileDefault::BeginPlay()
{
	Super::BeginPlay();

    BulletCollisionSphere->OnComponentHit.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereHit);
    BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereBeginOverlap);
    BulletCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereEndOverlap);
}

void AProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AProjectileDefault::InitProjectile(FProjectileInfo InitParam)
{
    this->SetLifeSpan(InitParam.ProjectileLifeTime);

    if (InitParam.ProjectileStaticMesh)
    {
        InitVisualMeshProjectile_Multicast(InitParam.ProjectileStaticMesh, InitParam.ProjectileStaticMeshOffset);
    }
    else
    {
        BulletMesh->DestroyComponent();
    }

    if (InitParam.ProjectileTrailFX)
    {
        InitVisualTrailProjectile_Multicast(InitParam.ProjectileTrailFX, InitParam.ProjectileTrailFxOffset);
    }
    else
    {
        BulletFX->DestroyComponent();
    }
    InitVelocity_Multicast(InitParam.ProjectileInitSpeed, InitParam.ProjectileMaxSpeed);

    ProjectileSetting = InitParam;
}

void AProjectileDefault::PostNetReceiveVelocity(const FVector &NewVelocity)
{
    if (BulletProjectileMovement)
    {
        BulletProjectileMovement->Velocity = NewVelocity;
    }
}

void AProjectileDefault::InitVelocity_Multicast_Implementation(float InitSpeed, float MaxSpeed)
{
    if (BulletProjectileMovement)
    {
        BulletProjectileMovement->Velocity = GetActorForwardVector() * InitSpeed;
        BulletProjectileMovement->InitialSpeed = InitSpeed;
        BulletProjectileMovement->MaxSpeed = MaxSpeed;
    }
}

void AProjectileDefault::ImpactProjectile()
{
    this->Destroy();
}

void AProjectileDefault::BulletCollisionSphereHit(UPrimitiveComponent *HitComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, FVector NormalImpulse, const FHitResult &Hit)
{
    if (OtherActor && Hit.PhysMaterial.IsValid())
    {
        EPhysicalSurface MySurfaceType = UGameplayStatics::GetSurfaceType(Hit);
        if (ProjectileSetting.HitDecals.Contains(MySurfaceType))
        {
            UMaterialInterface *myMaterial = ProjectileSetting.HitDecals[MySurfaceType];
            if (myMaterial && OtherComp)
            {
                SpawnHitDecal_Multicast(myMaterial, OtherComp, Hit);
            }
        }
        if (ProjectileSetting.HitFXs.Contains(MySurfaceType))
        {
            UParticleSystem *myParticle = ProjectileSetting.HitFXs[MySurfaceType];
            if (myParticle)
            {
                SpawnHitFX_Multicast(myParticle, Hit);
            }
        }
        if (ProjectileSetting.HitSound)
        {
            SpawnHitSound_Multicast(ProjectileSetting.HitSound, Hit);
        }
        UTypes::AddEffectBySurfaceType(Hit.GetActor(), Hit.BoneName, ProjectileSetting.StateEffect, MySurfaceType);
    }
    UGameplayStatics::ApplyPointDamage(OtherActor, ProjectileSetting.ProjectileDamage, Hit.TraceStart, Hit, GetInstigatorController(), this, NULL);
    UAISense_Damage::ReportDamageEvent(GetWorld(), Hit.GetActor(), GetInstigator(), ProjectileSetting.ProjectileDamage, Hit.Location, Hit.Location);
    ImpactProjectile();
}

void AProjectileDefault::BulletCollisionSphereBeginOverlap(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                                                           UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
}

void AProjectileDefault::BulletCollisionSphereEndOverlap(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex)
{
}

void AProjectileDefault::InitVisualMeshProjectile_Multicast_Implementation(UStaticMesh *NewMesh, FTransform MeshRelative)
{
    BulletMesh->SetStaticMesh(NewMesh);
    BulletMesh->SetRelativeTransform(MeshRelative);
}

void AProjectileDefault::InitVisualTrailProjectile_Multicast_Implementation(UParticleSystem *NewTemplate, FTransform TemplateRelative)
{
    BulletFX->SetTemplate(NewTemplate);
    BulletFX->SetRelativeTransform(TemplateRelative);
}

void AProjectileDefault::SpawnHitDecal_Multicast_Implementation(UMaterialInterface *DecalMaterial, UPrimitiveComponent *OtherComp, FHitResult HitResult)
{
    UGameplayStatics::SpawnDecalAttached(DecalMaterial, FVector(20.0f), OtherComp, NAME_None, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
}

void AProjectileDefault::SpawnHitFX_Multicast_Implementation(UParticleSystem *FxTemplate, FHitResult HitResult)
{
    UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxTemplate, FTransform(HitResult.ImpactNormal.Rotation(), HitResult.ImpactPoint, FVector(1.0f)));
}

void AProjectileDefault::SpawnHitSound_Multicast_Implementation(USoundBase *HitSound, FHitResult HitResult)
{
    UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, HitResult.ImpactPoint);
}