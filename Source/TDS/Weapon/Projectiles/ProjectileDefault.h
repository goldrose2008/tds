#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "FuncLibrary/Types.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "Perception/AISense_Damage.h"
#include "ProjectileDefault.generated.h"

UCLASS()
class TDS_API AProjectileDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	AProjectileDefault();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class UStaticMeshComponent *BulletMesh = nullptr;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class USphereComponent *BulletCollisionSphere = nullptr;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class UProjectileMovementComponent *BulletProjectileMovement = nullptr;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class UParticleSystemComponent *BulletFX = nullptr;

    UPROPERTY(BlueprintReadOnly)
    FProjectileInfo ProjectileSetting;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

    UFUNCTION(BlueprintCallable)
    void InitProjectile(FProjectileInfo InitParam);
    virtual void PostNetReceiveVelocity(const FVector &NewVelocity) override;
    UFUNCTION(NetMulticast, Reliable)
    void InitVelocity_Multicast(float InitSpeed, float MaxSpeed);
    UFUNCTION()
    virtual void ImpactProjectile();
    UFUNCTION()
    virtual void BulletCollisionSphereHit(class UPrimitiveComponent *HitComp, AActor *OtherActor,
                                  UPrimitiveComponent *OtherComp, FVector NormalImpulse, const FHitResult &Hit);
    UFUNCTION()
    void BulletCollisionSphereBeginOverlap(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                                           UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);
    UFUNCTION()
    void BulletCollisionSphereEndOverlap(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                                         UPrimitiveComponent *OtherComp, int32 OtherBodyIndex);
    UFUNCTION(NetMulticast, Reliable)
    void InitVisualMeshProjectile_Multicast(UStaticMesh* NewMesh, FTransform MeshRelative);
    UFUNCTION(NetMulticast, Reliable)
    void InitVisualTrailProjectile_Multicast(UParticleSystem *NewTemplate, FTransform TemplateRelative);
    UFUNCTION(NetMulticast, Reliable)
    void SpawnHitDecal_Multicast(UMaterialInterface *DecalMaterial, UPrimitiveComponent *OtherComp, FHitResult HitResult);
    UFUNCTION(NetMulticast, Reliable)
    void SpawnHitFX_Multicast(UParticleSystem *FxTemplate, FHitResult HitResult);
    UFUNCTION(NetMulticast, Reliable)
    void SpawnHitSound_Multicast(USoundBase *HitSound, FHitResult HitResult);
};
