#include "Weapon/Projectiles/ProjectileDefault_Grenade.h"

void AProjectileDefault_Grenade::BeginPlay()
{
    Super::BeginPlay();
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    TimerExplose(DeltaTime);
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent *HitComp, AActor *OtherActor,
                                                          UPrimitiveComponent *OtherComp, FVector NormalImpulse, const FHitResult &Hit)
{
    if (!bTimerEnabled)
    {
        Explose();
    }
    Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
    // Init Grenade
    bTimerEnabled = true;
}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
    if (bTimerEnabled)
    {
        if (TimerToExplose > TimeToExplose)
        {
            // Explose
            Explose();
        }
        else
        {
            TimerToExplose += DeltaTime;
        }
    }
}

void AProjectileDefault_Grenade::Explose()
{
    if (bShowDebug)
    {
        ShowHitDebugSphere_Multicast();
    }

    bTimerEnabled = false;
    if (ProjectileSetting.ExplodeFX)
    {
        SpawnHitFX_Grenade_Multicast(ProjectileSetting.ExplodeFX);
    }
    if (ProjectileSetting.ExplodeSound)
    {
        SpawnHitSound_Grenade_Multicast(ProjectileSetting.ExplodeSound);
    }

    TArray<AActor *> IgnoredActor;
    UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), ProjectileSetting.ExplodeMaxDamage, ProjectileSetting.ExplodeMaxDamage * 0.2f, GetActorLocation(),
                                                   ProjectileSetting.ProjectileMinRadiusDamage, ProjectileSetting.ProjectileMaxRadiusDamage, 5, NULL, IgnoredActor, this, nullptr);

    this->Destroy();
}

void AProjectileDefault_Grenade::ShowHitDebugSphere_Multicast_Implementation()
{
    DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Green, false, 12.0f);
    DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Red, false, 12.0f);
}

void AProjectileDefault_Grenade::SpawnHitFX_Grenade_Multicast_Implementation(UParticleSystem *FxTemplate)
{
    UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxTemplate, GetActorLocation(), GetActorRotation(), FVector(1.0f));
}

void AProjectileDefault_Grenade::SpawnHitSound_Grenade_Multicast_Implementation(USoundBase *HitSound)
{
    UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, GetActorLocation());
}