#pragma once

#include "CoreMinimal.h"
#include "Weapon/Projectiles/ProjectileDefault.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "ProjectileDefault_Grenade.generated.h"

UCLASS()
class TDS_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()
	
protected:
    virtual void BeginPlay() override;

public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
    bool bTimerEnabled = false;
    float TimerToExplose = 0.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
    float TimeToExplose = 3.0f;
    
    virtual void Tick(float DeltaTime) override;
    virtual void BulletCollisionSphereHit(class UPrimitiveComponent *HitComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, FVector NormalImpulse, const FHitResult &Hit) override;
    virtual void ImpactProjectile() override;
    void TimerExplose(float DeltaTime);
    void Explose();
    UFUNCTION(NetMulticast, Reliable)
    void ShowHitDebugSphere_Multicast();
    UFUNCTION(NetMulticast, Reliable)
    void SpawnHitFX_Grenade_Multicast(UParticleSystem *FxTemplate);
    UFUNCTION(NetMulticast, Reliable)
    void SpawnHitSound_Grenade_Multicast(USoundBase *HitSound);

    // -------------------------------------------------- DEBUG -------------------------------------------------------------------
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DEBUG")
    bool bShowDebug = true;
};