#include "Weapon/WeaponDefault.h"
#include "Character/TDSInventoryComponent.h"

AWeaponDefault::AWeaponDefault()
{
	PrimaryActorTick.bCanEverTick = true;

    // Network
    bReplicates = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
    RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Sceletal Mesh"));
    SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
    SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
    SkeletalMeshWeapon->SetupAttachment(RootComponent);


	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
    StaticMeshWeapon->SetGenerateOverlapEvents(false);
    StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
    StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
    ShootLocation->SetupAttachment(RootComponent);
}

void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

    WeaponInit();
}

void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    if (HasAuthority())
    {
        FireTick(DeltaTime);
        ReloadTick(DeltaTime);
        DispersionTick(DeltaTime);
        ClipDropTick(DeltaTime);
        ShellDropTick(DeltaTime);
    }
}
// -------------------------------------------------- Multiplayer --------------------------------------------
void AWeaponDefault::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AWeaponDefault, AdditionalWeaponInfo);
    DOREPLIFETIME(AWeaponDefault, bWeaponReloading);
    DOREPLIFETIME(AWeaponDefault, ShootEndLocation);
    DOREPLIFETIME(AWeaponDefault, bShouldReduceDispersion);
}

// -------------------------------------------------- Information for Weapon --------------------------------------------------
void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
        SkeletalMeshWeapon->DestroyComponent(true);
	}
    if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
    {
        StaticMeshWeapon->DestroyComponent(true);
    }
    UpdateStateWeapon_OnServer(EMovementState::Run_State);
}

void AWeaponDefault::UpdateStateWeapon_OnServer_Implementation(EMovementState NewMovementState)
{
    bBlockFire = false;

    switch (NewMovementState)
    {
    case EMovementState::Aim_State:
        bWeaponAiming = true;
        CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
        CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
        CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
        CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
        break;
    case EMovementState::AimWalk_State:
        bWeaponAiming = true;
        CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
        CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
        CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
        CurrentDispersionReduction = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionReduction;
        break;
    case EMovementState::Walk_State:
        bWeaponAiming = false;
        CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
        CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
        CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
        CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Walk_StateDispersionReduction;
        break;
    case EMovementState::Run_State:
        bWeaponAiming = false;
        CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
        CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
        CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
        CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Run_StateDispersionReduction;
        break;
    case EMovementState::SprintRun_State:
        bWeaponAiming = false;
        bBlockFire = true;
        SetWeaponStateFire_OnServer(false);
        break;
    default:
        break;
    }
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
    return WeaponSetting.ProjectileSetting;
}

int32 AWeaponDefault::GetNumberProjectileByShot() const
{
    return WeaponSetting.NumberProjectileByShot;
}

int32 AWeaponDefault::GetWeaponRound()
{
    return AdditionalWeaponInfo.Round;
}

// -------------------------------------------------- Fire ---------------------------------------------------------------------
void AWeaponDefault::Fire()
{
//On Server
    // Animation
    UAnimMontage *AnimToPlay = nullptr;
    if (bWeaponAiming)
    {
        AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharFireAim;
    }
    else
    {
        AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharFire;
    }
    if (WeaponSetting.AnimWeaponInfo.AnimWeaponFire)
    {
        AnimWeaponFire_Multicast(WeaponSetting.AnimWeaponInfo.AnimWeaponFire);
    }

    //drop Bullets and Shell
    if (WeaponSetting.ShellBullets.DropMesh)
    {
        if (WeaponSetting.ShellBullets.DropMeshTime < 0.0f)
        {
            InitDropMesh_OnServer(WeaponSetting.ShellBullets.DropMesh, WeaponSetting.ShellBullets.DropMeshOffset, WeaponSetting.ShellBullets.DropMeshImpulseDirection,
                WeaponSetting.ShellBullets.DropMeshLifeTime, WeaponSetting.ShellBullets.ImpulseRandomDispersion, WeaponSetting.ShellBullets.PowerImpulse,
                WeaponSetting.ShellBullets.CustomMass);
        }
        else
        {
            bDropShellFlag = true;
            DropShellTimer = WeaponSetting.ShellBullets.DropMeshTime;
        }
    }
    FireTimer = WeaponSetting.RateOfFire;
    AdditionalWeaponInfo.Round--;
    ChangeDispersionByShot();

    if (AnimToPlay)
    {
        OnWeaponFireStart.Broadcast(AnimToPlay);
    }

    FXWeaponFire_Multicast(WeaponSetting.EffectFireWeapon, WeaponSetting.SoundFireWeapon);

    int32 NumberProjectile = GetNumberProjectileByShot();

    if (ShootLocation)
    {
        FVector SpawnLocation = ShootLocation->GetComponentLocation();
        FRotator SpawnRotation = ShootLocation->GetComponentRotation();
        FProjectileInfo ProjectileInfo;
        ProjectileInfo = GetProjectile();

        FVector EndLocation;
        for (int8 i = 0; i < NumberProjectile; i++) //Shotgun
        {
            EndLocation = GetFireEndLocation();
// ���� ���� ��� ���������� - ������� ��������� (����\�������)
            if (ProjectileInfo.Projectile)
            {
                FVector Direction = EndLocation - SpawnLocation;

                Direction.Normalize();

                FMatrix MyMatrix(Direction, FVector(0, 0, 0), FVector(0, 0, 0), FVector::ZeroVector);
                SpawnRotation = MyMatrix.Rotator();

                FActorSpawnParameters SpawnParams;
                SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
                SpawnParams.Owner = GetOwner();
                SpawnParams.Instigator = GetInstigator();

                AProjectileDefault *MyProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
                if (MyProjectile)
                {
                    MyProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
                }
            }
            else
            {
// ���� ��� ���� ���������� - �������� ��������
                WeaponFireLineTrace_Multicast(SpawnLocation, EndLocation, WeaponSetting.DistanceTrace, ProjectileInfo.StateEffect);
            }
        }
    }

    if (GetWeaponRound() <= 0 && !bWeaponReloading)
    {
        InitReload();
    }
}

void AWeaponDefault::FireTick(float DeltaTime)
{
    if (bWeaponFiring && GetWeaponRound() > 0 && !bWeaponReloading)
    {
        if (FireTimer <= 0.0f)
        {
            Fire();
        }
        else
        {
            FireTimer -= DeltaTime;
        }
    }
}

void AWeaponDefault::SetWeaponStateFire_OnServer_Implementation(bool bIsFire)
{
    if (CheckWeaponCanFire())
    {
        bWeaponFiring = bIsFire;
    }
    else
    {
        bWeaponFiring = false;
        FireTimer = 0.01f;
    }
}

bool AWeaponDefault::CheckWeaponCanFire()
{
    return !bBlockFire;
}

FVector AWeaponDefault::GetFireEndLocation() const
{
    bool bShootDirection = false;
    FVector EndLocation = FVector(0.0f);

    FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
    if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
    {
        EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
        if (bShowDebug)
        {
            DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSetting.DistanceTrace,
                          GetCurrentDispersion() * PI / 180.0f, GetCurrentDispersion() * PI / 180.0f, 32, FColor::Emerald, false, 10.0f, (uint8)'\000', 1.0f);
        }
    }
    else
    {
        EndLocation = ShootLocation->GetForwardVector() * 20000.0f;
        if (bShowDebug)
        {
            DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.f,
                          GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, 10.0f, (uint8)'\000', 1.0f);
        }
    }

    if (bShowDebug)
    {
        // direction weapon look
        DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.0f, (uint8)'\000', 0.5f);
        // direction projectile must fly
        DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.0f, (uint8)'\000', 0.5f);
        // Direction Projectile Current fly
        DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.0f, (uint8)'\000', 0.5f);
    }

    return EndLocation;
}

void AWeaponDefault::UpdateWeaponByCharacterMovementState_OnServer_Implementation(FVector NewShootEndLocation, bool bNewShouldReduceDispersion)
{
    ShootEndLocation = NewShootEndLocation;
    bShouldReduceDispersion = bNewShouldReduceDispersion;
}

void AWeaponDefault::AnimWeaponFire_Multicast_Implementation(UAnimMontage* AnimFire)
{
    if (AnimFire && SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
    {
        SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(AnimFire);
    }
}

void AWeaponDefault::ShellDropFire_Multicast_Implementation(UStaticMesh *DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh,
                                                            float ImpulseRandomDispersion, float PowerImpulse, float CustomMass, FVector LocalDirection)
{
    AStaticMeshActor *NewActor = nullptr;
    NewActor = GetWorld()->SpawnActorDeferred<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Offset, this, nullptr, ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn);

    if (NewActor && NewActor->GetStaticMeshComponent())
    {
        NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
        NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
        NewActor->SetActorTickEnabled(false);
        NewActor->InitialLifeSpan = LifeTimeMesh;
        NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
        NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
        NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

        NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
        NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
        NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
        NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
        NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
        NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);
        UGameplayStatics::FinishSpawningActor(NewActor, Offset);

        if (CustomMass > 0.0f)
        {
            NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
        }

        if (!DropImpulseDirection.IsNearlyZero())
        {
            FVector FinalDirection;
            LocalDirection = LocalDirection + (DropImpulseDirection * 1000.0f);

            if (!FMath::IsNearlyZero(ImpulseRandomDispersion))
            {
                FinalDirection +=
                    UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDirection, ImpulseRandomDispersion);
            }
            else
            {
                FinalDirection.GetSafeNormal(0.0001f);
            }

            NewActor->GetStaticMeshComponent()->AddImpulse(FinalDirection * PowerImpulse);
        }
    }
}

void AWeaponDefault::FXWeaponFire_Multicast_Implementation(UParticleSystem *FxFire, USoundBase *SoundFire)
{
    if (FxFire)
    {
        UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxFire, ShootLocation->GetComponentTransform());
    }
    if (SoundFire)
    {
        UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SoundFire, ShootLocation->GetComponentLocation());
    }
}

void AWeaponDefault::WeaponFireLineTrace_Multicast_Implementation(FVector LocationStart, FVector LocationEnd, float DistanceTrace, TSubclassOf<UTDS_StateEffect> EffectHit)
{
    if (bShowDebug)
    {
        DrawDebugLine(GetWorld(), LocationStart, LocationStart + ShootLocation->GetForwardVector() * DistanceTrace, FColor::Black, false, 5.0f, (uint8)'\000', 0.5f);
    }
    FHitResult Hit;
    TArray<AActor *> Actors;
    UKismetSystemLibrary::LineTraceSingle(GetWorld(), LocationStart, LocationEnd * DistanceTrace, ETraceTypeQuery::TraceTypeQuery4, false,
                                          Actors, EDrawDebugTrace ::ForDuration, Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);

    if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
    {
        EPhysicalSurface MySurfaceType = UGameplayStatics::GetSurfaceType(Hit);

        if (WeaponSetting.ProjectileSetting.HitDecals.Contains(MySurfaceType))
        {
            UMaterialInterface *MyMaterial = WeaponSetting.ProjectileSetting.HitDecals[MySurfaceType];

            if (MyMaterial && Hit.GetComponent())
            {
                SpawnHitTraceDecal_Multicast(MyMaterial, Hit.GetComponent(), Hit);
            }
        }

        if (WeaponSetting.ProjectileSetting.HitFXs.Contains(MySurfaceType))
        {
            UParticleSystem *MyParticle = WeaponSetting.ProjectileSetting.HitFXs[MySurfaceType];

            if (MyParticle)
            {
                SpawnHitTraceFX_Multicast(MyParticle, Hit);
            }
        }

        if (WeaponSetting.ProjectileSetting.HitSound)
        {
            SpawnHitTraceSound_Multicast(WeaponSetting.ProjectileSetting.HitSound, Hit);
        }

        UTypes::AddEffectBySurfaceType(Hit.GetActor(), Hit.BoneName, EffectHit, MySurfaceType);
// ��������� ������ �� ��������
        UGameplayStatics::ApplyDamage(Hit.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage, GetInstigatorController(), this, NULL);
    }
}

void AWeaponDefault::SpawnHitTraceFX_Multicast_Implementation(UParticleSystem *FxTemplate, FHitResult HitResult)
{
    UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxTemplate, FTransform(HitResult.ImpactNormal.Rotation(), HitResult.ImpactPoint, FVector(1.0f)));
}

void AWeaponDefault::SpawnHitTraceSound_Multicast_Implementation(USoundBase *HitSound, FHitResult HitResult)
{
    UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, HitResult.ImpactPoint);
}

void AWeaponDefault::SpawnHitTraceDecal_Multicast_Implementation(UMaterialInterface *DecalMaterial, UPrimitiveComponent *OtherComp, FHitResult HitResult)
{
    UGameplayStatics::SpawnDecalAttached(DecalMaterial, FVector(20.0f), OtherComp, NAME_None, HitResult.ImpactPoint,
                                         HitResult.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
}

// -------------------------------------------------- Dispersia-------------------------------------------------------

void AWeaponDefault::DispersionTick(float DeltaTime)
{
    if (!bWeaponReloading)
    {
        if (!bWeaponFiring)
        {
            if (bShouldReduceDispersion)
            {
                CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
            }
            else
            {
                CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
            }
        }
        if (CurrentDispersion < CurrentDispersionMin)
        {
            CurrentDispersion = CurrentDispersionMin;
        }
        else
        {
            if (CurrentDispersion > CurrentDispersionMax)
            {
                CurrentDispersion = CurrentDispersionMax;
            }
        }
    }
    if (bShowDebug)
    {
        //UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
    }
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
    return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.0f);
}

void AWeaponDefault::ChangeDispersionByShot()
{
    CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
    float Result = CurrentDispersion;
    return Result;
}

// -------------------------------------------------- Reload -------------------------------------------------------------------
void AWeaponDefault::ReloadTick(float DeltaTime)
{
    if (bWeaponReloading)
    {
        if (ReloadTimer < 0.0f)
        {
            FinishReload();
        }
        else
        {
            ReloadTimer -= DeltaTime;
        }
    }
}

void AWeaponDefault::InitReload()
{
//On Server
    if (CheckCanWeaponReload())
    {
        bWeaponReloading = true;
        ReloadTimer = WeaponSetting.ReloadTime;

        UAnimMontage *AnimToPlay = nullptr;
        UAnimMontage *AnimWeaponToPlay = nullptr;
        if (bWeaponAiming)
        {
            AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharReloadAim;
            AnimWeaponToPlay = WeaponSetting.AnimWeaponInfo.AnimWeaponReloadAim;
        }
        else
        {
            AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharReload;
            AnimWeaponToPlay = WeaponSetting.AnimWeaponInfo.AnimWeaponReload;
        }

        OnWeaponReloadStart.Broadcast(AnimToPlay);
        if (WeaponSetting.AnimWeaponInfo.AnimWeaponReload && SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
        {
            AnimWeaponFire_Multicast(AnimWeaponToPlay);
        }

        UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundReloadWeapon, ShootLocation->GetComponentLocation());

        if (WeaponSetting.ClipDropMesh.DropMesh)
        {
                bDropClipFlag = true;
                DropClipTimer = WeaponSetting.ClipDropMesh.DropMeshTime;
        }
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("AWeaponDefault::InitReload - Reload is not possible"));
    }
}

void AWeaponDefault::FinishReload()
{
    bWeaponReloading = false;
    int32 AvailableAmmoFromInventory = GetAvailableAmmoForReload();
    int32 AmmoNeedTakeFromInventory;
    int32 NeedToReload = WeaponSetting.MaxRound - AdditionalWeaponInfo.Round;

    if (NeedToReload > AvailableAmmoFromInventory)
    {
        AdditionalWeaponInfo.Round += AvailableAmmoFromInventory;
        AmmoNeedTakeFromInventory = AvailableAmmoFromInventory;
    }
    else
    {
        AdditionalWeaponInfo.Round += NeedToReload;
        AmmoNeedTakeFromInventory = NeedToReload;
    }

    OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInventory);
}

void AWeaponDefault::CancelReload()
{
    bWeaponReloading = false;
    if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
    {
        SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);
    }
    OnWeaponReloadEnd.Broadcast(false, 0);
    bDropClipFlag = false;
}

bool AWeaponDefault::CheckCanWeaponReload()
{
    bool bResult = true;

    if (GetOwner())
    {
        UTDSInventoryComponent *MyInventory = Cast<UTDSInventoryComponent>(GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
        if (MyInventory)
        {
            int32 AvailableAmmoForWeapon;
            if (!MyInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType, AvailableAmmoForWeapon))
            {
                bResult = false;
                MyInventory->OnWeaponNotHaveRoundEvent_Multicast(MyInventory->GetWeaponIndexSlotByName(IdWeaponName));
            }
            else
            {
                MyInventory->OnWeaponHaveRoundEvent_Multicast(MyInventory->GetWeaponIndexSlotByName(IdWeaponName));
            }
        }
    }

    return bResult;
}

int32 AWeaponDefault::GetAvailableAmmoForReload()
{
    int32 AvailableAmmoForWeapon = WeaponSetting.MaxRound;
    if (GetOwner())
    {
        UTDSInventoryComponent *MyInventory = Cast<UTDSInventoryComponent>(GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
        if (MyInventory)
        {
            MyInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType, AvailableAmmoForWeapon);
        }
    }

    return AvailableAmmoForWeapon;
}

void AWeaponDefault::ClipDropTick(float DeltaTime)

{
    if (bDropClipFlag)
    {
        if (DropClipTimer < 0.0f)
        {
            bDropClipFlag = false;
            InitDropMesh_OnServer(WeaponSetting.ClipDropMesh.DropMesh, WeaponSetting.ClipDropMesh.DropMeshOffset, WeaponSetting.ClipDropMesh.DropMeshImpulseDirection, WeaponSetting.ClipDropMesh.DropMeshLifeTime,
                         WeaponSetting.ClipDropMesh.ImpulseRandomDispersion, WeaponSetting.ClipDropMesh.PowerImpulse, WeaponSetting.ClipDropMesh.CustomMass);
        }
        else
        {
            DropClipTimer -= DeltaTime;
        }
    }
}

void AWeaponDefault::ShellDropTick(float DeltaTime)
{
    if (bDropShellFlag)
    {
        if (DropShellTimer < 0.0f)
        {
            bDropShellFlag = false;
            InitDropMesh_OnServer(WeaponSetting.ShellBullets.DropMesh, WeaponSetting.ShellBullets.DropMeshOffset, WeaponSetting.ShellBullets.DropMeshImpulseDirection, WeaponSetting.ShellBullets.DropMeshLifeTime,
                         WeaponSetting.ShellBullets.ImpulseRandomDispersion, WeaponSetting.ShellBullets.PowerImpulse, WeaponSetting.ShellBullets.CustomMass);
        }
        else
        {
            DropShellTimer -= DeltaTime;
        }
    }
}

void AWeaponDefault::InitDropMesh_OnServer_Implementation(UStaticMesh *DropMesh, FTransform Offset, FVector DropImpulseDirection,
                                                         float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass)
{
    if (DropMesh)
    {
        FTransform Transform;
        FVector LocalDirection = this->GetActorForwardVector() * Offset.GetLocation().X +
                                 this->GetActorRightVector() * Offset.GetLocation().Y +
                                 this->GetActorUpVector() * Offset.GetLocation().Z;
        Transform.SetLocation(GetActorLocation() + LocalDirection);
        Transform.SetScale3D(Offset.GetScale3D());
        Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());

        ShellDropFire_Multicast(DropMesh, Transform, DropImpulseDirection, LifeTimeMesh, ImpulseRandomDispersion, PowerImpulse, CustomMass, LocalDirection);
    }
}