#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "FuncLibrary/Types.h"
#include "Weapon/Projectiles/ProjectileDefault.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMeshActor.h"
#include "Net/UnrealNetwork.h"
#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage *, AnimFireChar);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, AnimReloadChar);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);

UCLASS()
class TDS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	AWeaponDefault();

    FOnWeaponFireStart OnWeaponFireStart;
    FOnWeaponReloadStart OnWeaponReloadStart;
    FOnWeaponReloadEnd OnWeaponReloadEnd;

// -------------------------------------------------- Components ---------------------------------------------
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
    class USceneComponent *SceneComponent = nullptr;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
    class USkeletalMeshComponent *SkeletalMeshWeapon = nullptr;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
    class UStaticMeshComponent *StaticMeshWeapon = nullptr;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
    class UArrowComponent *ShootLocation = nullptr;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

// -------------------------------------------------- Multiplayer --------------------------------------------
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const override;

// -------------------------------------------------- Information for Weapon --------------------------------------------------
    UPROPERTY(VisibleAnywhere, Category = "Weapon Info")
    FWeaponInfo WeaponSetting;
    UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
    FAdditionalWeaponInfo AdditionalWeaponInfo;

    void WeaponInit();
    UFUNCTION(Server, Reliable)
    void UpdateStateWeapon_OnServer(EMovementState NewMovementState);
    FProjectileInfo GetProjectile();
    int32 GetNumberProjectileByShot() const;
    UFUNCTION(BlueprintCallable)
    int32 GetWeaponRound();

// -------------------------------------------------- Fire ---------------------------------------------------------------------
    UPROPERTY(Replicated)
    FVector ShootEndLocation = FVector(0);
    float FireTimer = 0.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
    bool bWeaponFiring = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
    FName IdWeaponName;
    bool bBlockFire = false;

    void Fire();
    void FireTick(float DeltaTime);
    UFUNCTION(Server, Reliable, BlueprintCallable)
    void SetWeaponStateFire_OnServer(bool bIsFire);
    bool CheckWeaponCanFire();
    FVector GetFireEndLocation() const;
    UFUNCTION(Server, Unreliable)
    void UpdateWeaponByCharacterMovementState_OnServer(FVector NewShootEndLocation, bool bNewShouldReduceDispersion);
    UFUNCTION(NetMulticast, Unreliable)
    void AnimWeaponFire_Multicast(UAnimMontage *AnimFire);
    UFUNCTION(NetMulticast, Unreliable)
    void ShellDropFire_Multicast(UStaticMesh *DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, 
                                 float PowerImpulse, float CustomMass, FVector LocalDirection);
    UFUNCTION(NetMulticast, Unreliable)
    void FXWeaponFire_Multicast(UParticleSystem* FxFire, USoundBase* SoundFire);
    UFUNCTION(NetMulticast, Unreliable)
    void WeaponFireLineTrace_Multicast(FVector LocationStart, FVector LocationEnd, float DistanceTrace, TSubclassOf<UTDS_StateEffect> EffectHit);
    UFUNCTION(NetMulticast, Unreliable)
    void SpawnHitTraceFX_Multicast(UParticleSystem *FxTemplate, FHitResult HitResult);
    UFUNCTION(NetMulticast, Unreliable)
    void SpawnHitTraceSound_Multicast(USoundBase *HitSound, FHitResult HitResult);
    UFUNCTION(NetMulticast, Unreliable)
    void SpawnHitTraceDecal_Multicast(UMaterialInterface *DecalMaterial, UPrimitiveComponent *OtherComp, FHitResult HitResult);


// -------------------------------------------------- Dispersion ---------------------------------------------------------------
    UPROPERTY(Replicated) 
    bool bShouldReduceDispersion = false;
    float CurrentDispersion = 0.0f;
    float CurrentDispersionMax = 1.0f;
    float CurrentDispersionMin = 0.1f;
    float CurrentDispersionRecoil = 0.1f;
    float CurrentDispersionReduction = 0.1f;

    void DispersionTick(float DeltaTime);
    FVector ApplyDispersionToShoot(FVector DirectionShoot) const;
    void ChangeDispersionByShot();
    float GetCurrentDispersion() const;

// -------------------------------------------------- Aiming -------------------------------------------------------------------
    bool bWeaponAiming = false;

    // -------------------------------------------------- Reload -------------------------------------------------------------------
    UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
    bool bWeaponReloading = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
    float ReloadTimer = 0.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic Debug") // Remove !!! Debug
    float ReloadTime = 0.0f;
    bool bDropClipFlag = false;
    float DropClipTimer = -1.0f;
    bool bDropShellFlag = false;
    float DropShellTimer = -1.0f;

    void ReloadTick(float DeltaTime);
    void InitReload();
    void FinishReload();
    void CancelReload();
    bool CheckCanWeaponReload();
    int32 GetAvailableAmmoForReload();
    void ClipDropTick(float DeltaTime);
    void ShellDropTick(float DeltaTime);
    UFUNCTION(Server, Reliable)
    void InitDropMesh_OnServer(UStaticMesh *DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh,
                      float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);

// -------------------------------------------------- DEBUG -------------------------------------------------------------------
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DEBUG")
    bool bShowDebug = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DEBUG")
    float SizeVectorToChangeShootDirectionLogic = 100.0f;
};
